RNW=$(wildcard Rnw/*.Rnw)$

all: main nonlinear versioncontrol mixed bootstrap multivariate tidy

basic: Rnotes_main.pdf Rnotes_nonlinear.pdf Rnotes_versioncontrol.pdf Rnotes_bootstrap.pdf Rnotes_multivariate.pdf Rnotes_mixed.pdf  tidy

main: Rnotes_main.pdf Rnotes_main-print.pdf 
nonlinear: Rnotes_nonlinear-print.pdf Rnotes_nonlinear.pdf 
versioncontrol: Rnotes_versioncontrol-print.pdf Rnotes_versioncontrol.pdf
bootstrap: Rnotes_bootstrap-print.pdf Rnotes_bootstrap.pdf 
multivariate: Rnotes_multivariate-print.pdf Rnotes_multivariate.pdf 
mixed: Rnotes_mixed-print.pdf Rnotes_mixed.pdf 

Rnotes_%-print.pdf: Rnotes_%.pdf 
	gs -o $@ -dNoOutputFonts -sDEVICE=pdfwrite $<

Rnotes_%.pdf: Rnotes_%.tex 
	pdflatex $<
	pdflatex $<
	
Rnotes_%.tex: Rnotes_%.Rnw $(RNW)
	Rscript -e "knitr::knit('$<')"

tidy:
	rm -f *.log *.aux *.bbl *.blg *.fls *.out *.snm *.nav *.tdo *.toc *.id* *.toc *.ilg *.toc *.ind

clean: 
	rm -f *.pdf *.tex

.PHONY: all clean
