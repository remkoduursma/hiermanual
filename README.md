# Data analysis and Visualization with R

This repository includes all files necessary to compile the 'HIE R manual', a book written for the purpose of teaching an [R course at the Hawkesbury Institute for the Environment](http://www.westernsydney.edu.au/hie/opportunities/training_courses/data_analysis_r). 

The unofficial site for teaching this course is here: [www.hiercourse.com](http://www.hiercourse.com).

Please note that these files are released under the [GPL-2 license](http://choosealicense.com/licenses/gpl-2.0/). You may use the work, but please attribute this repository.

This manual is written in LaTeX and [knitr](http://yihui.name/knitr/) and requires [ghostscript](https://www.ghostscript.com/) to be installed and available in the search path. See 'Compiling the manual' further below for important information.



### Setting up

- Many R packages are needed to run all code in this manual. Run `source("install_package_dependencies.R")` to install them all (you may need to run it twice for some dependencies to be resolved).

- You need an installation of LaTeX. On Windows, install MikTex and make sure that missing packages are installed 'on the fly'. 


### Compiling the manual

- The manual is compiled using the `make` command, as specified in the [Makefile](./Makefile). To build everything, type 

```
make
``` 
in a terminal window (tested on Windows and Mac). Or to build a particular target run:

```
make main
```

Outputs come in two flavors, for example `Rnotes_main.pdf` (small PDF for sharing on the web, fonts embedded), `Rnotes_main-print.pdf` (very large PDF for printing safely without font dependencies - see 'Printing the manual' below).
Various documents are built this way, including the "main" version used for the 5-day course, as well as standalone documents for various workshops (mixed effects, multivariate, version control etc.). If you want to set up a new custom document (with any selection of chapters), see any of the `*.Rnw` files (for example, `Rnotes_nonlinear.Rnw`).

At the moment the solutions to exercises are not compiled with the Makefile, for those open the corresponding Rnw document in Rstudio and press 'Knit'.

## Notes to contributors to the manual


### R code conventions

- Do not use `=` for assignment, use only `<-` 

- Don't worry too much about formatting the code nicely, as the `formatR` package does that for you, as part of the `knitr` package.

- Comment (nearly) every line of code in a code chunk, even if it repeats the text just above it.

- When loading a package that prints a lot of startup mess, use this template to suppress that:
First run this chunk,
```
#!r
<<echo=FALSE>>=
suppressPackageStartupMessages(library(multcomp))
@
```
Then run your actual code, and use `library` again.
```
#!r
<<>>=
library(multcomp)
@
```
 
The last load will print nothing, because the package is already loaded. Alternatively one might set `message=FALSE` as a chunk option, but this would suppress all messages.


### LaTeX conventions

- As a reminder, escape underscores!, so `\texttt{endophytes_env.csv}` must be `\texttt{endophytes\_env.csv}`

- When referring to variables in a dataframe, always use `\code{varname}`, like you do for R functions.

- Refer to R using `\R{}`

- Conventions (use these terms, not a close alternative)
```
#!latex
 p-value
 $t$-test
 $F$-statistic
```

- For all code chunks that produce figures, use the following template, or similar:
   You MUST include a caption for each figure.
 
```
#!r
<<distplot, fig.cap='Two univariate distributions plotted with curve()', opts.label="smallsquare">>=
...
@
```
Then just above the code block, refer to the figure like this:
```
#!latex
The following code produces Fig.~\ref{fig:distplot}. 
```

Note the use of `opts.label` in the chunk option. Each main Rnw document includes the available templates for figures, for example for `Rnotes_main`, these are 'smallsquare', 'wide', 'largesquare' and 'extrawide'. Using these settings ensures that all plots are consistent in size. 


- Refer to sections frequently, like this : `by the way, see Section~\ref{sec:somesectionlabel}`. The section label is done with the usual `label{sec:somesection}` just below the section start.

- Always refer to a dataset on first use with the appendix like this:
   `we will use the Age and Memory dataset (see Section~\ref{sec:agemem})`

- Use the following custom environments to help make the index work:
```
#!latex
 \fun{}   is a function, add to index
 \pack{}  is a package, add to index
 \FUN{}  is the main entry for the function, bold index
 \PACK{}  is the main entry for a package, bold index
 \code{}   code - not added to index (functions, packages, anything that used to be \texttt)
 \index{}   add to index - e.g. index functions in a code chunk, just above the chunk.
 \file{}   a filename (don't quote! \file{myfile.csv})
```

- 'TRY THIS YOURSELF' info box.  A custom environment 'trybox' is defined for this. Below is an example of its use. You CAN NOT include a code chunk within a box! But you can use `\code` and the verbatim environment.

```
#!latex
 \begin{trybox}
 Install the \code{hello} package. And also,
 \begin{verbatim}
 x <- 10
 y <- 20
 \end{verbatim}
 \end{trybox}
```

- 'Further Reading' box. A custom environment to refer to references with more background reading, other packages, etc. 
```
\begin{furtherreading}
Also see this guy (Guy, 2001)
\end{furtherreading}
```

### Printing the manual

When printing the manual, we keep having problems with the code chunks (although not on our own machines, only when by third party printing services). The solution is to convert the fonts to outlines (glyphs), using Ghostscript.

The solution can be read from [this Stackoverflow page](http://stackoverflow.com/questions/28797418/replace-all-font-glyphs-in-a-pdf-by-converting-them-to-outline-shapes). With newer versions of Ghostscript, use:

```
gs -o RnotesNO.pdf -dNoOutputFonts -sDEVICE=pdfwrite Rnotes.pdf
```

This has been tested with GhostScript version 9.19 only, and only on Windows. 

It might be useful to run [pdffonts](http://www.foolabs.com/xpdf/download.html) on the resulting PDF to double check that no fonts are present (since older versions of Ghostscript run the above command but don't actually remove fonts).

**Note:** if you are using the Makefile (as described above, and as highly recommended!), this conversion is already done for you.







