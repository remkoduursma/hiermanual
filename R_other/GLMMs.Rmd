Generalised linear mixed-effects models (GLMMs)
========================================================
*Jeff Powell* - `r format(Sys.time(),"%Y-%m-%d")`

We have seen how to fit generalised linear models and linear mixed models. Once you know how to fit these models in R, fitting GLMMs is fairly easy using the `glmer` function in the `lme4` package. 

The file `eucfaceGC.csv` contains estimates of plant and litter cover within the rings of the EucFACE experiment, evaluating forest ecosystem responses to elevated CO2, on two dates. There are six rings (`Ring`), three for each treatment (`Trt`; ambient and elevated CO2). Within each ring are four plots (`Plot`) and within each plot are four 1m by 1m subplots (`Sub`). Here we will test for an interaction between `Trt` and `Date` on `Forbes` cover (these are count data). 

```{r}

# read data and convert random effects to factors
eucface <- read.csv('eucfaceGC.csv')
eucface$Ring <- factor(paste(eucface$Ring, eucface$Trt, sep='-'))
eucface$Plot <- factor(eucface$Plot)
eucface$Sub <- factor(eucface$Sub)
str(eucface)

library(lme4)
library(lattice)

# plot data
bwplot(Forbes~Date|Trt,data=eucface)
# try this to observe values colour coded by each plot within a ring
xyplot(Forbes~Date|Ring,groups=Plot,data=eucface,pch=16,jitter.x=T)

```

The residuals are clearly not normally distributed, but let's look at a diagnostic plot of a fit using a gaussian distribution anyway.

```{r}

forb.norm <- lmer(Forbes~Date*Trt+(1|Ring/Plot/Sub), data=eucface)
qqnorm(resid(forb.norm))  # Q-Q plot
qqline(resid(forb.norm))  # add line

```

The residuals deviate quite strongly from the normal expectation (the line on the Q-Q plot). Since the data best resemble count data, let's try the poisson distribution.

```{r}

# fit model with interaction between Date and Trt and random effects
forb.pois <- glmer(Forbes~Date*Trt+(1|Ring/Plot/Sub), family=poisson, data=eucface)
# diagnostic plot
qqnorm(resid(forb.pois))
qqline(resid(forb.pois))

```

These residuals look better than the previous ones, but they are still not very good. Let's try changing the `link` function for the poisson family from `log` to `sqrt`.

```{r}

forb.pois.sqrt <- glmer(Forbes~Date*Trt+(1|Ring/Plot/Sub), family=poisson(sqrt), data=eucface)
# diagnostic plot
qqnorm(resid(forb.pois.sqrt))
qqline(resid(forb.pois.sqrt))

```

The result is slightly better but still not great. The fit of the model may be improved further by using one of the other plant cover variables as a covariate or by incorporating other data from the site, but for our purposes we will continue on from here.

Something else to consider when it comes to poisson errors is that overdispersion and can result in the underestimation of error terms for the model coefficients. This may be due to a large number of zeros in the data and/or an important predictor not being accounted for. A good model fit should result in the ratio of residual deviance to degrees of freedom being close to 1. 

```{r}

# residual deviance
sum(resid(forb.pois.sqrt, type='pearson')^2)

# model summary
summary(forb.pois.sqrt)

```

Overdispersion does not appear to be a problem here. If it was, we could use a quasipoisson family as for GLMs but it is not clear whether it is appropriate to extend quasi-families to GLMMs. It has also been suggested that including individual-level random effects in the model could alleviate the problem; we will try this as an example.

```{r}

eucface$Ind <- factor(1:nrow(eucface))  # individual-level random effects
forb.pois.ind <- glmer(Forbes~Date*Trt+(1|Ring/Plot/Sub/Ind), family=poisson(sqrt), data=eucface)
summary(forb.pois.ind)

```

The random effects block indicates that a small amount of variance is accounted for by the individual level random effects.  

Based on the `bwplot` above and the `allEffects` plot below, there may be an interaction between `Date` and `Trt`. 

```{r}

library(effects)
plot(allEffects(forb.pois.ind))

```

Next, let's fit the same model without this interaction term and compare the two models.

```{r}

# drop the interaction term from the model
drop1(forb.pois.ind)

```

The AIC score is lower for the full model, suggesting that the interaction term is an important predictor. We can also use `Anova` function from the `car` package to get a P-value associated with this hypothesis (this is significant).

```{r}

library(car)
Anova(forb.pois.ind)

```

The next question is 'what is the source of the interaction?' It appears that forbs decreased in abundance between the two dates in the control but not in the elevated CO2 treatment and that forb abundance on the second date, but not the first, was lower in the control than in the elevated CO2 treatment. We can evaluate this through model selection after combining treatment levels.

```{r}

# create a three level factor that combines both dates in the `elev` treatment
eucface$trtcomb.elev <- with(eucface, paste(Trt, Date, sep='-'))
eucface$trtcomb.elev[eucface$Trt == 'elev'] <- 'elev'
eucface$trtcomb.elev <- factor(eucface$trtcomb.elev)
levels(eucface$trtcomb.elev)

# create a three level factor that combines both treatments for the '11/06/13' sampling
eucface$trtcomb.date <- with(eucface, paste(Trt, Date, sep='-'))
eucface$trtcomb.date[eucface$Date == '11/06/13'] <- '11/06/13'
eucface$trtcomb.date <- factor(eucface$trtcomb.date)
levels(eucface$trtcomb.date)

# rename full model for brevity
m1 <- forb.pois.ind
# fit model with three level combination factors
m2.elev <- glmer(Forbes ~ trtcomb.elev + (1|Ring/Plot/Sub/Ind), family=poisson(sqrt), data=eucface)
m2.date <- glmer(Forbes ~ trtcomb.date + (1|Ring/Plot/Sub/Ind), family=poisson(sqrt), data=eucface)
# fit models with only one main effect (Date or Trt)
m3.Trt <- glmer(Forbes~Date+(1|Ring/Plot/Sub/Ind), family=poisson(sqrt), data=eucface)
m3.Date <- glmer(Forbes~Trt+(1|Ring/Plot/Sub/Ind), family=poisson(sqrt), data=eucface)
# compare models
AIC(m1, m2.elev, m2.date, m3.Trt, m3.Date)

```

The three level model in which treatment effects are only observed on the second date (`m2.date`) has the lowest AIC, so is the model that we select as the one that best predicts forb dynamics. However, the fit of this model is still suspect, for the reasons given above, and we should be skeptical of the result. 
