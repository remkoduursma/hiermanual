# clear workspace
rm(list=ls())

# set working directory
setwd('/Users/jeffpowell/RProjects/hiermanual/data')

### One-way ANOVA

coweeta <- read.csv('coweeta.csv')
head(coweeta)
boxplot(height ~ species, data=coweeta)

# generate subset with four species
cowsub <- droplevels(subset(coweeta, species %in% c('cofl', 'bele', 'oxar', 'quru')))
boxplot(height ~ species, data=cowsub)

# fit model and look at model summary
m1 <- lm(height ~ species, data=cowsub)
summary(m1)

# generate ANOVA table
anova(m1)

# perform multiple comparisons
library(multcomp)
?glht
?mcp
tukey.species <- glht(m1, linfct=mcp(species='Tukey'))
summary(tukey.species)
plot(tukey.species)
par(mar=c(5, 7, 4, 1))
plot(tukey.species)

# dunnett.species <- glht(m1, linfct=mcp(species='Dunnett'))
# summary(dunnett.species)
# summary(m1)


### Two-way ANOVA

memory <- read.table('eysenck.txt', header=T)
summary(memory)
xtabs( ~ Age + Process, data=memory)
library(lattice)
bwplot(Words ~ Process | Age, data=memory)
memory$Process <- with(memory, reorder(Process, Words, mean))
bwplot(Words ~ Process | Age, data=memory)

# test for main effects and interactions
m1 <- lm(Words ~ Process + Age, data=memory)
anova(m1)
summary(m1)
library(visreg)
visreg(m1, 'Process', by='Age', overlay=T)

m2 <- lm(Words ~ Process * Age, data=memory)
anova(m2)
summary(m2)
visreg(m2, 'Process', by='Age', overlay=T)

# check model diagnostics, come back to this
plot(m2, which=2)
plot(m2, which=3)


### multiple linear regression

allom <- read.csv('Allometry.csv')
head(allom)

# plot relationships
par(mfrow=c(1, 2))
plot(leafarea ~ diameter, data=allom)
plot(leafarea ~ height, data=allom)
plot(leafarea ~ diameter, data=allom, log='xy')
plot(leafarea ~ height, data=allom, log='xy')

# apply log transformation
allom$logLA <- log10(allom$leafarea)
allom$logD <- log10(allom$diameter)
allom$logH <- log10(allom$height)

# fit model, check diagnostics, and look at model summary
m1 <- lm(logLA ~ logD + logH, data=allom)
plot(m1, which=c(3, 2))
summary(m1)


### continuous and categorical predictors

summary(allom)

m2 <- lm(logLA ~ logD * species + logH * species, data=allom)
plot(m2, which=c(3, 2))
summary(m2)
visreg(m2, 'logD', by='species', overlay=T)
visreg(m2, 'logH', by='species', overlay=T)

# compare sequential and marginal sums of squares
anova(m2)
m3 <- lm(logLA ~ logH * species + logD * species, data=allom)
anova(m3)
drop1(m2, test='F')
library(car)
Anova(m2)


### Generalised linear models

?family

m1 <- lm(Words ~ Process * Age, data=memory)
m2 <- glm(Words ~ Process * Age, data=memory, family=poisson)
plot(m1, which=2)
plot(m2, which=2)
anova(m2)
?anova.glm
anova(m2, test='LRT')
summary(m2)

par(mfrow=c(1, 2))
visreg(m1, 'Process', by='Age', overlay=T)
visreg(m2, 'Process', by='Age', overlay=T)


# logistic regression (two ways to do this)

titanic <- read.table('titanic.txt', header=T)
str(titanic)

# generate a plot with a categorical response
titanic$status <- factor(ifelse(titanic$Survived==1, 'alive', 'dead'))
plot(status ~ Sex, data=titanic)
plot(status ~ PClass, data=titanic)
plot(status ~ Age, data=titanic)
boxplot(Age ~ status, data=titanic)

# individual binary responses
m1 <- glm(Survived ~ Age + Sex + PClass, data=titanic, family=binomial)
summary(m1)
library(effects)
plot(allEffects(m1, by='Age'))


# # tabular data (only works when all predictors are categorical)
# titanic$AgeGrp <- factor(ifelse(titanic$Age > 18, 'adult', 'child'))
# str(titanic)
# titanic2 <- aggregate(cbind(Survived==1, Survived==0) ~ AgeGrp + Sex + PClass, 
#                       data=titanic, FUN=sum)
# titanic2
# names(titanic2)
# names(titanic2)[4:5] <- c('survived', 'died')
# names(titanic2)
# m2 <- glm(cbind(survived, died) ~ AgeGrp + Sex + PClass, data=titanic2, family=binomial)
# summary(m2)

