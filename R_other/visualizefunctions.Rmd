---
title: "Chapter 6 Visualized"
author: "Remko Duursma"
date: "April 15 2015"
output: 
  ioslides_presentation:
    widescreen: true
    smaller: false
    logo: img/hielogo.png
    
---

## Example data

![](../screenshots/exampledata.png)

```{r eval=FALSE}
# Read CSV file into a dataframe
plantdat <- read.csv("somedatafile.csv")
```


## Calculating group means (1)

```{r eval=FALSE}
# tapply results in a vector
with(plantdat, tapply(Plantbiomass, Treatment, mean))
```

Results in a **vector**: 

![](../screenshots/tapplyresult.png)

Note that a vector can also have names (just like the columns of a dataframe).


## Calculating group means (2)

```{r eval=FALSE}
# summaryBy results in a dataframe
library(doBy)
summaryBy(Plantbiomass ~ treatment, FUN=mean, data=plantdat)
```

Results in a **dataframe**:

![](../screenshots/summaryByresult.png)


## Calculating group means (3)

What if we have another factor in the dataframe:

![](../screenshots/exampledatalarger.png)

## Calculating group means (4)

Now, `tapply` results in a **matrix**

```{r eval=FALSE}
with(plantdat, tapply(Plantbiomass, list(Species, Treatment), mean))
```

![](../screenshots/tapplyresultlarger.png)

Where **A** and **B** are the *rownames* of the matrix.

## Calculating group means (5)

But `summaryBy` still results in a dataframe:

```{r eval=FALSE}
summaryBy(Plantbiomass ~ Species + Treatment, FUN=mean, data=dfr)
```

![](../screenshots/summaryByresultlarger.png)


## Merging datasets (1)

We received a dataset with leaf nitrogen data for each plant:

![](../screenshots/leafnitrogendata.png)

Note that the data are not ordered, and data for one plant is missing.

The data can be merged like so:

```{r eval=FALSE}
plantdat <- merge(plantdat, leafNdat, by="PlantID")
```

## Merging datasets (2)

Which results in: 

![](../screenshots/mergeresult.png)

Note the missing value where data were not available,
and that the order of the second dataset has no effect.

## Row-binding data

If (and only if!) you have two datasets with the same columns, they can
be directly row-binded.

More data:

![](../screenshots/moredataforrbind.png)

```{r eval=FALSE}
rbind(plantdat, moredata)
```

## Row-binding data (2)

Which results in :

![](../screenshots/rbindresult.png)














