---
title: "Intro to glmer"
author: "Remko Duursma"
date: "4 April 2017"
output: ioslides_presentation
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, message=FALSE)
```

## Generalized Linear Mixed Effects Models

- Linear Mixed Effects (lme) models assume normal (gaussian) errors
- In Generalized lme (glme) models, we can choose a number of other error distributions 'families'
- The most common ones are *binomial* (logistic regression) and *poisson* (typical for count data)
- The *link function* specifies how the mean response is related to the predictors.
- The choice of link function and error family gives a wide array of possible models.


## Fitting a glmer

We fit a `glmer` using the `lme4` package. We just have to specify one extra argument:

```{r, eval=FALSE, echo=TRUE}
myfit <- glmer(response ~ predictors + (1|randomvar), 
               family=poisson, data=mydata)
```

Where `family` can be one of `poisson` or `binomial`, and a number of others (see `?family`).

To change the link function, we can use `family = poisson(link="sqrt")`, for example.


## Logistic regression

For logistic regression (i.e., `family=binomial`) the data can be organized in two ways.

Option 1 : the data are specified as 0 (failure) and 1 (success). The variable can be a factor variable where the *second level* is the success.

For example, for Titanic survival data:

```{r, echo=FALSE}
tit <- read.delim("../data/titanic.txt", header=T)
head(tit[,c("Name","PClass","Survived")], 4)
```

In this case we can use `Survived` as the response variable in the model specification.

## Logistic regression (2)

Option 2 : the data are already tabulated into number of successes and failures for each row of data.
For example, the number of germinated (`germ`) seeds in a batch (of size `n`) were counted:

```{r, echo=FALSE}
seedfire <- read.csv("../data/germination_fire.csv")
head(seedfire[,c("species","germ","n")],4)
```

In this case we can use the following specification:

```{r, eval=FALSE, echo=TRUE}
fit1 <- glmer(cbind(germ,n) ~ species, data=seedfire)
```



## Poisson regression {.smaller}

- The Poisson distribution expresses the probability of a given number of events occurring in some time interval, characterized by a single parameter $\lambda$, the 'rate' parameter.
- The Poisson family is commonly used with count data because a) they are bounded by zero, b) they are discrete. 
- For large $\lambda$, the normal distribution gives a very good approximation to the Poisson.

```{r, echo=FALSE, fig.height=3.5, fig.width=6}
par(mar=c(3,3,1,1), cex=0.9, mgp=c(2,0.5,0), tcl=0.2)
x <- 1:40

plot(x, dpois(x, lambda=2), type='o', pch=21, bg="cornflowerblue", cex=1.1,
     xlab="X", ylab="Probability density")
points(x, dpois(x, lambda=5), type='o', pch=21, bg="forestgreen", cex=1.1)
points(x, dpois(x, lambda=25), type='o', pch=21, bg="orange", cex=1.1)
legend("topright", as.character(c(2,5,25)), title=expression(lambda), 
       fill=c("cornflowerblue","forestgreen","orange"))

```




```{r, echo=FALSE}
eucface <- read.csv("../data/eucfaceGC.csv")
eucface$Ring <- as.factor(paste(eucface$Ring, eucface$Trt, sep='-'))
eucface$Plot <- as.factor(eucface$Plot)
eucface$Sub <- as.factor(eucface$Sub)

library(lme4)
forb.pois.sqrt <- glmer(Forbes~Date*Trt+(1|Ring/Plot/Sub), 
                        family=poisson('sqrt'), data=eucface)
```



## Diagnostics (1) {.smaller}

- For both Poisson and binomial regression the residual deviance is expected to be $\chi^2$ distributed with the appropriate degrees of freedom. In practice, the sum of the squared Pearson residuals should roughly equal the residual degrees of freedom.
- An exception is given for logistic regression when the response is 0,1 (dispersion makes no sense).

```{r, echo=TRUE}
# Sum of squared Pearson residuals
sum(residuals(forb.pois.sqrt, type="pearson")^2)

# ... must be approx. equal to df of the residual:
df.residual(forb.pois.sqrt)
```


## Diagnostics (2) {.smaller}

- Best is to check if the distribution assumed in the model is 'equal' to the distribution in the data.
- The `DHARMa` package take a simulation-based approach to this problem.

```{r, echo=TRUE}
library(DHARMa)
simr <- simulateResiduals(forb.pois.sqrt, 500)
plotSimulatedResiduals(simr)
```


## Understanding effects {.smaller}

- For logistic regression, the link function is logit, which means that all effects are expressed not as a probability (P) of success but rather:
$$y = log(\frac{p}{1-p}) $$
- For meaningful interpretation these have to be backtransformed.
- With `visreg`, set the argument `scale = "response"` 
- With `predict`, set `type = "response"`




## A word of caution

- `glmer` models do not always 'converge', especially for complex (random effect) designs.
- There are many alternative implementations to `glmer` in R, `lme4` may not always be the best.

![](../screenshots/glmm_difficult_bolker.png)















