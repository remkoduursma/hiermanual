%---------------------------------------------------------------------------%
% Copyright 2015 Remko Duursma, Jeff Powell                                 %
%                                                                           %
% This file is part of HIERmanual                                           %
%                                                                           %
%     HIERmanual is free software: you can redistribute it and/or modify    %
%     it under the terms of the GNU General Public License as published by  %
%     the Free Software Foundation, either version 3 of the License, or     %
%     (at your option) any later version.                                   %
%                                                                           % 
%     HIERmanual is distributed in the hope that it will be useful,         %
%     but WITHOUT ANY WARRANTY; without even the implied warranty of        %
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         %
%     GNU General Public License for more details.                          %
%                                                                           %
%     You should have received a copy of the GNU General Public License     %
%     along with HIERmanual.  If not, see <http://www.gnu.org/licenses/>.   %
%---------------------------------------------------------------------------%

\documentclass{report}
\usepackage{rnotesformat}

% Useful when editing - print section/figure/eqn labels in pdf
%\usepackage[inline]{showlabels}

\makeindex[columns=3]

\begin{document}

<<child="Rnw/rnotes_knitr_options.rnw">>=
@


%-- Title page --%
\title{Data analysis and Visualisation with R}

\begin{titlepage}

%\begin{center}

%\HRule \\[0.4cm]
{ \Huge \textbf{A Learning Guide to R}}\\[0.4cm]
{ \large \textit{Beginner to intermediate skills in data analysis, visualization, and manipulation}} \\
\HRule \\[0.5cm]

%\end{center}

\large{Remko Duursma, Jeff Powell \& Glenn Stone}\\
\vfill

%\begin{center}
\includegraphics[width=0.25\textwidth]{titlepage/WSUlogo.pdf}\\[1cm]


%\end{center}




\end{titlepage}


%-- Preface --%
<<child="Rnw/00_Preface.Rnw", eval=TRUE>>=
@


%-- Table of contents --%
\tableofcontents


% Set knitr theme (cannot be set in knitr options for some reason!)
<<echo=FALSE>>=
# See http://animation.r-forge.r-project.org/knitr/
library("knitr")
opts_knit$set(out.format = "latex")
knit_theme$set("earendel")
opts_chunk$set(background="grey94") #, tidy.opts=list(width.cutoff=70))
opts_knit$set(root.dir="data")
@

% Avoid long output
% https://github.com/yihui/knitr-examples/blob/master/052-suppress-output.md
<<echo=FALSE>>=
hook_output = knit_hooks$get("output")
knit_hooks$set(output = function(x, options) {
    if (!is.null(n <- options$out.lines)) {
        x = unlist(stringr::str_split(x, "\n"))
        if (length(x) > n) {
            # truncate the output
            x = c(head(x, n), "....\n")
        }
        x = paste(x, collapse = "\n")  # paste first n lines together
    }
    hook_output(x, options)
})
@

% Figure size templates
<<echo=FALSE>>=
opts_template$set(smallsquare = list(fig.height = 5, fig.width = 5, fig.align="center", out.width=".6\\linewidth"),
                  wide        = list(fig.height = 5, fig.width = 9, fig.align="center", out.width=".8\\linewidth"),
                  largesquare = list(fig.height = 8, fig.width = 8, fig.align="center", out.width=".8\\linewidth"),
                  extrawide = list(fig.height = 4, fig.width = 10, fig.align="center", out.width=".9\\linewidth"))
@


%-- Chapters --%

<<child="Rnw/01_BasicsOfR.Rnw", eval=TRUE>>=
@
<<child="Rnw/02_generatingdata.Rnw", eval=TRUE>>=
@
<<child="Rnw/03_SpecialDataTypes.Rnw", eval=TRUE>>=
@
<<child="Rnw/04_BasicPlotting.Rnw", eval=TRUE>>=
@
<<child="Rnw/05_BasicStats.Rnw", eval=TRUE>>=
@
<<child="Rnw/06_SummarizeData.Rnw", eval=TRUE>>=
@
<<child="Rnw/07_LinearModelling.Rnw", eval=TRUE>>=
@
%<<child="Rnw/08_MultivariateStatistics.Rnw", eval=TRUE>>=
%@
%<<child="Rnw/09_MixedEffects.Rnw", eval=TRUE>>=
%@
<<child="Rnw/10_WorkingLists.Rnw", eval=TRUE>>=
@
<<child="Rnw/11_projectman.Rnw", eval=TRUE>>=
@
<<child="Rnw/12_Hints.Rnw", eval=TRUE>>=
@
%<<child="Rnw/13_ErrorsInR.Rnw", eval=TRUE>>=
%@
<<child="Rnw/14_GettingHelp.Rnw", eval=TRUE>>=
@



<<child="Rnw/emptypage.Rnw", eval=TRUE>>=
@

%-- Appendix --%

\appendix

<<child="Rnw/15_DataDescriptions.Rnw", eval=TRUE>>=
@

<<child="Rnw/99_Epilogue.Rnw", eval=TRUE>>=
@

\newpage

\indexprologue{\noindent List of nearly all functions used in this book. Packages are in \textbf{bold}. Bold page numbers refer to the key description of the function.}
\printindex


\end{document}







