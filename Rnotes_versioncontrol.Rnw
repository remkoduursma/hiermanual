%---------------------------------------------------------------------------%
% Copyright 2015 Remko Duursma, Jeff Powell                                 %
%                                                                           %
% This file is part of HIERmanual                                           %
%                                                                           %
%     HIERmanual is free software: you can redistribute it and/or modify    %
%     it under the terms of the GNU General Public License as published by  %
%     the Free Software Foundation, either version 3 of the License, or     %
%     (at your option) any later version.                                   %
%                                                                           % 
%     HIERmanual is distributed in the hope that it will be useful,         %
%     but WITHOUT ANY WARRANTY; without even the implied warranty of        %
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         %
%     GNU General Public License for more details.                          %
%                                                                           %
%     You should have received a copy of the GNU General Public License     %
%     along with HIERmanual.  If not, see <http://www.gnu.org/licenses/>.   %
%---------------------------------------------------------------------------%

\documentclass{report}

\usepackage{rnotesformat}
\usepackage{caption}
\captionsetup{skip=0pt}

\externaldocument{Rnotes}

\makeindex

\begin{document}

<<child="Rnw/rnotes_knitr_options.rnw">>=
@



%-- Title page --%
\title{Data analysis and Visualisation with R}

\begin{titlepage}

%\begin{center}

%\HRule \\[0.4cm]
{ \Huge \textbf{Version control with git and Rstudio}}\\[0.4cm]
\HRule \\[0.5cm]

%\end{center}

\large{Remko Duursma}\\
\vfill

%\begin{center}
\includegraphics[width=0.25\textwidth]{titlepage/WSUlogo.pdf}\\[1cm]

{\today}

%\end{center}




\end{titlepage}


%-- Preface --%
% <<child="Rnw/00_PrefaceVol2.Rnw", eval=TRUE>>=
% @


%-- Table of contents --%
\tableofcontents


% Set knitr theme (cannot be set in knitr options for some reason!)
<<echo=FALSE>>=
# See http://animation.r-forge.r-project.org/knitr/
knit_theme$set("earendel")
opts_chunk$set(background="grey94") #, tidy.opts=list(width.cutoff=70))
opts_knit$set(root.dir="data")

knit_hooks$set(
smallsquarepar = function(before, options, envir) {
    if (before) par(mar=c(4,4,1,1), mgp=c(2.5,0.8,0))
}, 
widepar = function(before, options, envir) {
    if (before) par(mar=c(4,4,1,1), mgp=c(2.5,0.8,0))
}
)

opts_template$set(
  smallsquare = list(fig.height = 4, fig.width = 4, 
                     fig.align="center", smallsquarepar=TRUE),
  wide = list(fig.height=4, fig.width=8, 
              fig.align="center", widepar=TRUE))
@


%-- Chapters --%


<<child="Rnw/18_versioncontrol.Rnw", eval=TRUE>>=
@



\newpage
\printindex


\end{document}





