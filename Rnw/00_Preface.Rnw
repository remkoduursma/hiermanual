%---------------------------------------------------------------------------%
% Copyright 2015 Remko Duursma, Jeff Powell                                 %
%                                                                           %
% This file is part of HIERmanual                                           %
%                                                                           %
%     HIERmanual is free software: you can redistribute it and/or modify    %
%     it under the terms of the GNU General Public License as published by  %
%     the Free Software Foundation, either version 3 of the License, or     %
%     (at your option) any later version.                                   %
%                                                                           % 
%     HIERmanual is distributed in the hope that it will be useful,         %
%     but WITHOUT ANY WARRANTY; without even the implied warranty of        %
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         %
%     GNU General Public License for more details.                          %
%                                                                           %
%     You should have received a copy of the GNU General Public License     %
%     along with HIERmanual.  If not, see <http://www.gnu.org/licenses/>.   %
%---------------------------------------------------------------------------%


\section*{Prologue}

This book is not a complete introduction to statistical theory. It should also not be the first statistics book you read. Instead, this book shows you how to implement many useful data analysis routines in \R{}. Sometimes we explain a bit of theory behind the method, but this is an exception. We really assume that you have already learned statistics elsewhere and are here to learn how to actually do the analyses.

We have learned from experience that data practictioners, that means scientists and everyone else who has an interest in learning from data, do not learn statistical analyses by studying the underlying theory. They learn from doing, and from using examples written by others who have learned by doing. For this reason the book in front of you is largely a compendium of examples. We have chosen the examples and the exercises in the hope that they resemble real-world problems. 

We have designed this book so it can be used for self-study. The exercises at the end of each chapter aim to test important skills learned in the chapter. Before you start on the exercises, read through the text, try to run the examples, and play around with the code making small modifications. We have also placed many \emph{Try it yourself} boxes throughout the text to give you some ideas on what to try, but try to be creative and modify the examples to see what happens.

You can download the example datasets used in this book (see Appendix~\ref{sec:datasets}), as well as the solutions to the exercises at \url{bit.ly/RDataCourse}.

An index with the functions and packages used is provided at the end of this book.



\subsection*{What is data analysis?}

Most scientists would answer that this is the part of the work where the p-values are calculated, perhaps alongside a host of other metrics and tables. We take the view that \emph{data analysis includes every step from raw data to the outputs}, where outputs include figures, tables and statistics. This view is summarized in the figure on the next page.

Because every step from raw data to output affects the outcome, every step should be well documented, and reproducible. By 'reproducible', we mean it should be possible for some other person (even if this is the future you) to re-run the analysis and get the same results. We agree with many others that \R{} is particularly good for reproducible research of this kind \footnote{Gandrud, C. Reproducible Research with R and R Studio. CRC Press, 2015. 2nd Edition.}, because it has utilities for all steps of data analysis.

We stress that it is absolutely vital to a successful data analysis to visualize at every step of the analysis \footnote{Anscombe 1973 "Graphs in Statistical analysis", The American Statistician 27:17-21}. When analyzing data, you should apply a continuous loop of statistical inference and visualizing the data (Find a significant effect you did not expect? Visualize it!; Visually find something interesting? Analyze it!).


\begin{figure}[h!]
  \centering
  \includegraphics[width=0.6\textwidth]{screenshots/dataanalysishr.png}
  \label{fig:dataanalysis}
  \caption{A broader view of data analysis. The view we take in this book is that all operations on the data should be viewed as part of data analysis. Usually, only the statistical analysis is viewed as the analysis, but in many applications there are numerous steps from the initial raw data (produced from some data source, perhaps an experiment or an observational study) leading to the eventual figures and statistics. One of these steps is the combining of several raw data files into a processed dataset, which may also include data cleaning, calculation of new variables, renaming of columns, reshaping, and so on. Derived datasets are common as well, and these include summary variables (e.g. averages by treatment, totals over some time period). The process of visualizing the data via figures, and testing for significant effects and other statistical procedures is a two-way street: figures inform the statistics to be applied and vice versa (uncovering statistical significance on some effect mandates visualizing the effect). We stress to all data practitioners that all steps of data analysis are implemented in such a way to be fully reproducable, and documented in its entirety.}
\end{figure}



% \subsubsection*{Approaching a complex analysis}
% 
% It is not our place to give specific advise on how your analysis should proceed. The details will differ depending on the data, the field of study, and the specific objectives. We do like to give a few tips on how you could proceed:
% 
% \begin{itemize}
% \item There are always many ways in which a specific problem can be solved. Compare results (main conclusions, not precise p-values) with different approaches. If very different conclusions are reached, you should worry and seek advise. If similar conclusions are reached, this substantially increases confidence in the conclusions.
% \item It is often forgotten or perhaps ignored that the analysis of a complex dataset will be invariably complex itself. It is nearly always useful to start simple, and slowly build in complexity as you start to understand the nature of the dataset. First perform simplified analyses on subsets of data (e.g. only two treatments, or a single date of measurement, ignoring random effects). Compare the analysis of the overall dataset with simple analyses of subsets. The most important concept: KISS (Keep It Simple, Stupid).
% \item Approaches to data analysis can be quite different for experimental or observational data. In the former, a strict application of treatments mandates a particular analysis, whereas in the latter case it seems you could get away with nearly anything. Either way, a more successful approach is to have preconceived ideas to test with the data, rather than to mine the data for patterns, p-values, and fit complex models with little meaning. Complex statistics is no alternative to thinking about your data.
% \end{itemize}


\subsection*{Assumed skills of the reader}

This book is written for scientists and data analysts who do not have a background in computer science, mathematics, or statistics. The astute \R{} programmer might notice that some terminology has been simplified, for reasons of clarity. Experience with programming in any language is therefore not required. We do assume that the reader has completed at least an \emph{Introduction to Statistics} course, as this book does not explain the fundamentals of statistical analysis. 

If you have used \R{} before, you will very likely still learn many new skills, and perhaps understand the more basic skills a little better.

\newpage

\subsection*{About the Authors}

Remko Duursma was an Associate Professor at the Hawkesbury Institute for the Environment, Western Sydney University, and is now searching for a new direction as an independent data scientist. His research at HIE focused on understanding vegetation function and structure in response to the environment, using models and data. In his research, he routinely analyses large datasets arising from sensor networks, climate change experiments, literature syntheses, and other sources. He has been using R since 2001. 

Jeff Powell is an Associate Professor based at the Hawkesbury Institute for the Environment at Western Sydney University. His research interests centre on understanding community assembly processes, particularly in the context of soil ecology and plant-microbe interactions, and relating these to ecosystem function. He utilises a variety of approaches to ecological informatics approach in his research, developing new tools and extending existing tools to test ecological hypotheses with large datasets from surveys of ecological communities, ecosystem properties, and species and environmental attributes. He has been using R since 2005.

Glenn Stone is a statistician and data scientist currently working in the private sector. Previously he has been a Professor of Data Science in the Centre for Research in Mathematics at Western Sydney University and, before that, a Principal Research Analyst with Insurance Australia Group and a Senior Research Statistician with the CSIRO. His research interests are in computational statistics and data science, as applied to a wide variety of scientific and commercial applications. He has been using R since 2003.


\subsection*{Acknowledgments}

This book accompanies the five-day course 'Data analysis and visualization with R', taught twice a year at the Hawkesbury Institute for the Environment (\url{www.westernsydney.edu.au/hie}). The PDF of this book is available at \url{www.westernsydney.edu.au/rmanual}. The class website can be reached at \url{bit.ly/RDataCourse}.

We have used \pack{knitr} (with Sweave and {\LaTeX}) (see \url{http://yihui.name/knitr/}) to produce this book, from within Rstudio (\url{www.rstudio.com}). We also use numerous add-on packages. Thanks to the developers for making these tools freely available.

Thanks also to those who have contributed original datasets that are used throughout this book (see Appendix~\ref{sec:datasets}).

{\today}





