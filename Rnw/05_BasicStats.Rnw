%---------------------------------------------------------------------------%
% Copyright 2015 Remko Duursma, Jeff Powell                                 %
%                                                                           %
% This file is part of HIERmanual                                           %
%                                                                           %
%     HIERmanual is free software: you can redistribute it and/or modify    %
%     it under the terms of the GNU General Public License as published by  %
%     the Free Software Foundation, either version 3 of the License, or     %
%     (at your option) any later version.                                   %
%                                                                           % 
%     HIERmanual is distributed in the hope that it will be useful,         %
%     but WITHOUT ANY WARRANTY; without even the implied warranty of        %
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         %
%     GNU General Public License for more details.                          %
%                                                                           %
%     You should have received a copy of the GNU General Public License     %
%     along with HIERmanual.  If not, see <http://www.gnu.org/licenses/>.   %
%---------------------------------------------------------------------------%

<<echo=FALSE, cache=FALSE, results="hide">>=
set.seed(1234)
@



\chapter{Basic statistics}

\label{chap:basic}

This book is not an \emph{Introduction to statistics}. There are many manuals (either printed or on the web) that document the vast array of statistical analyses that can be done with \R{}. To get you started, though, we will show a few very common analyses that are easy to do in \R{}. 

In all of the following, we assume you have a basic understanding of linear regression, Student's $t$-tests, ANOVA, and confidence intervals for the mean.

\section{Probability Distributions}
\label{sec:distributions}

As we assume you have completed an \emph{Introduction to statistics} course, you have already come across several probability distributions. For example, the \emph{Binomial} distribution is a model for the distribution of the number of \emph{successes} in a sequence of independent trials, for example, the number of heads in a coin tossing experiment. Another commonly used discrete distribution is the \emph{Poisson}, which is a useful model for many kinds of count data. Of course, the most important distribution of all is 
the \emph{Normal} or Gaussian distribution.

\R{} provides sets of functions to find densities, cumulative probabilities, quantiles, and to draw random numbers from many important distributions. The names of the functions all consist of a one letter prefix that specifies the type of function and a stem which specifies the distribution. Look at the examples in the table below.

\begin{minipage}{0.49\textwidth}\begin{center}
\begin{tabular}{|l|l|}
\hline
prefix&function\\ 
\hline\hline
\code{d}& density\\ \hline
\code{p}& cumulative probabilities\\ \hline
\code{q}& quantiles\\ \hline
\code{r}& simulate\\ \hline
\end{tabular}\end{center}
\end{minipage}
\begin{minipage}{0.49\textwidth}\begin{center}
\begin{tabular}{|l|l|}
\hline
stem&distribution\\ 
\hline\hline
\code{binom}& Binomial\\ \hline
\code{pois}& Poisson\\ \hline
\code{norm}& Normal\\ \hline
\code{t}& Student's t\\ \hline
\code{chisq}& $\chi^2$\\ \hline
\code{f}& F \\ \hline
\end{tabular}\end{center}
\end{minipage}

So for example, 
<<>>=
# Calculate the probability of 3 heads out of 10 tosses of a fair coin.
# This is a (d)ensity of a (binom)ial distribution.
dbinom(3, 10, 0.5)

# Calculate the probability that a normal random variable (with 
# mean of 3 and standard deviation of 2) is less than or equal to 4.
# This is a cumulative (p)robability of a (norm)al variable.
pnorm(4, 3, 2)

# Find the t-value that corresponds to a 2.5% right-hand tail probability
# with 5 degrees of freedom.
# This is a (q)uantile of a (t)distribution.
qt(0.975, 5)

# Simulate 5 Poisson random variables with a mean of 3. 
# This is a set of (r)andom numbers from a (pois)son distribution.
rpois(5, 3)
@

See the help page \code{?Distributions} for more details.

To make a quick plot of a distribution, we already saw the use of the density function in combination with \fun{curve} in Section~\ref{sec:hist}. Here is another example (this makes Fig.~\ref{fig:distplot}).

<<distplot, fig.cap='Two univariate distributions plotted with curve()', opts.label="smallsquare">>=
# A standard normal distribution
curve(dnorm(x, sd=1, mean=0), from=-3, to=3,
      ylab="Density", col="blue")

# Add a t-distribution with 3 degrees of freedom.
curve(dt(x, df=3), from =-3, to=3, add=TRUE, col="red")

# Add a legend (with a few options, see ?legend)
legend("topleft", c("Standard normal","t-distribution, df=3"), lty=1, col=c("blue","red"),
       bty='n', cex=0.8)
@


\begin{trybox}
Make a histogram (recall Section~\ref{sec:hist}) of a sample of random numbers from a distribution of your choice.
\end{trybox}

\section{Descriptive Statistics}
\label{sec:descstat}

Descriptive statistics summarise some of the properties of a given data set. Generally, we are interested in measures of location (central tendency, such as mean and median) and scale (variance or standard deviation). Other descriptions can include the sample size, the range, and so on. We already encountered a number of functions that can be used to summarize a vector. 

Let's look at some examples for the Pupae dataset (described in Section~\ref{sec:pupaedata}).
\index{mean} \index{var} \index{length}
<<>>=
# Read data
pupae <- read.csv("pupae.csv")

# Extract the weights (for convenience)
weight <- pupae$PupalWeight

# Find the number of observations
length(weight)

# Find the average (mean) weight
mean(weight)

# Find the Variance
var(weight)
@

Note that \R{} will compute the sample variance (not the population variance). The standard deviation can be calculated as the square root of the variance, or use the \FUN{sd} function directly.

<<>>=
# Standard Deviation
var.wgt <- var(weight)
sqrt(var.wgt)

# Standard Deviation
sd(weight)
@

Robust measures of the location and scale are the median and inter-quartile range; \R{} has functions for these.

<<>>=
# median and inter-quartile range
median(weight)
IQR(weight)
@

The median is the 50th percentile or the second quartile. The \fun{quantile} 
function can compute quartiles as well as arbitrary percentiles/quantiles.

<<>>=
quantile(weight)

quantile(weight, probs=seq(0,1,0.1))
@

\textbf{Missing Values}: All of the above functions will return \code{NA} if the data contains \emph{any} missing values. However, they also provide an option to remove missing values (\code{NA}s) before their computations (see also Section~\ref{sec:workingmissing}).

<<>>=
weightNA <- weight
weightNA[40] <- NA
mean(weightNA)
mean(weightNA, na.rm=TRUE)
@

The \fun{summary} function provides a lot of the above information in a single command:
<<>>=
summary(weight)
@

The \pack{moments} package provides 'higher moments' if required, for example, 
the \fun{skewness} and \fun{kurtosis}.
<<>>=
# load the moments package
library(moments)
skewness(weight)
kurtosis(weight)
@

The \pack{pastecs} package includes a useful function that calculates many descriptive statistics for numeric vectors, including the standard error for the mean (for which \R{} has no built-in function).

<<include=FALSE>>=
library(pastecs)
@

<<>>=
library(pastecs)

# see ?stat.desc for description of the abbreviations
stat.desc(weight)

# conveniently, the output is a character vector which we can index by name,
# for example extracting the standard error for the mean
stat.desc(weight)["SE.mean"]
@


Sometimes you may wish to calculate descriptive statistics for subgroups in the data. We will come back to this extensively in Section~\ref{sec:tapplyaggregate}, but here is a quick introduction. 

The function \fun{tapply} allows you to apply any function to subgroups defined by a second (grouping) variable, as we will see in Chapter~\ref{chap:summarize}. 
<<>>=
# tapply
with(pupae, tapply(PupalWeight, Gender, mean))
@


\section{Inference for a single population}
\label{sec:inference}

\emph{Inference} is answering questions about population parameters based on a sample. The mean of a random sample from a population is an estimate of the population mean. Since it is a single number it is called a point estimate. It is often desirable to estimate a range within which the population parameter lies with high probability. This is called a confidence interval.

One way to get confidence intervals in \R{} is to use the quantile functions for the relevant distribution. Remember from your introductory statistics course that a $100(1-\alpha)$\% confidence interval for the mean on normal population is given by,

\[\bar{x} \pm t_{\alpha/2, n-1} \frac{s}{\sqrt{n}}\]

where $\bar{x}$ is the sample mean, $s$ the sample standard deviation and $n$ is the sample size. $t_{\alpha/2, n-1}$ is the $\alpha/2$ tail point of a $t$-distribution on $n-1$ degrees of freedom. That is, if $T$ has a $t$-distribution on $n-1$ degrees of freedom.

\[P(T \leq t_{\alpha/2, n-1}) = 1-\alpha/2 \]

The \R{} code for this confidence interval can be written as,

<<>>=
alpha <- 0.05 # 95% confidence interval
xbar <- mean(weight)
s <- sd(weight)
n <- length(weight)
half.width <- qt(1-alpha/2, n-1)*s/sqrt(n)

# Confidence Interval 
c(xbar - half.width, xbar + half.width)
@

Here, we assumed a normal distribution for the population. You may have been taught that if $n$ is \emph{large}, say $n>30$, then you can use a normal approximation. That is, replace \code{qt(1-alpha/2, n-1)} with \code{qnorm(1-alpha/2)}, but there is no need, \R{} can use the $t$-distribution for any $n$ (and the results will be the same, as the $t$-distribution converges to a normal distribution when the df is large).

\begin{trybox}
Confirm that the $t$-distribution converges to a normal distribution when $n$ is large (using \code{qt} and \code{qnorm}).
\end{trybox}


\subsection*{Hypothesis testing}

There may be a reason to ask whether a dataset is consistent with a certain mean. For example, are the pupae weights consistent with a population mean of 0.29? For normal populations, we can use Student's $t$-test, available in \R{} as the \FUN{t.test} function. Let's test the null hypothesis that the population mean is 0.29:

<<>>=
t.test(weight, mu=0.29)
@

Note that we get the $t$-statistic, degrees of freedom ($n-1$) and a p-value for the test, with the specified alternative hypothesis (not equal, i.e. two-sided). In addition, \fun{t.test} gives us a 95\% confidence interval (compare to the above), and the estimated mean, $\bar{x}$.

We can use \code{t.test} to get any confidence interval, and/or to do one-sided tests,

<<>>=
t.test(weight, mu=0.29, conf.level=0.90)
t.test(weight, mu=0.29, alternative="greater", conf.level=0.90)
@

Note that the confidence interval is one-sided when the test is one-sided.

The \code{t.test} is appropriate for data that is approximately normally distributed. You can check this using a histogram or a QQ-plot (see Sections~\ref{sec:hist} and~\ref{sec:diagplots}). If the data is not very close to a normal distribution then 
the \fun{t.test} is often still appropriate, as long as the sample is large.

If the data is not normal and the sample size is small, there are a couple of alternatives: transform the data (often a log transform is enough) or use a \emph{nonparametric} test, in this case the Wilcoxon signed rank test. We can use the \fun{wilcox.test} function for the latter, its interface is similar to \fun{t.test} and it tests the hypothesis that the data is symmetric about the hypothesized population mean. For example,

<<>>=
wilcox.test(weight, mu=0.29)
wilcox.test(weight, mu=0.29, alternative="greater")
@

\subsubsection*{Test for proportions}

Sometimes you want to test whether observed proportions are consistent with a hypothesized population proportion. For example, consider a coin tossing experiment where you want to test the hypothesis that you have a fair coin (one with an equal probability of landing heads or tails). In your experiment, you get  60 heads out of 100 coin tosses. Do you have a fair coin? We can use the \fun{prop.test} function:

<<>>=
# 60 'successes' out of a 100 trials, the hypothesized probability is 0.5.
prop.test(x=60, n=100, p=0.5)

# Same as above, but for a one-sided test.
prop.test(60, 100, p=0.5, alternative="greater")
@

\emph{Note:} You might think that I chose to do a one-sided test \emph{greater} 
because 60 is greater than the expected number of 50 for a fair coin -- I didn't! Don't use your data to decide on the null or alternative hypothesis -- it renders the p-values meaningless.


\section{Inference for two populations}

Commonly, we wish to compare two (or more) populations. For example, the \code{pupae} dataset has pupal weights for female and male pupae. We may wish to compare the weights of males (gender=0) and females (gender=1). 

There are two ways to use \fun{t.test} to compare the pupal weights of males and females. In the first method, we make two vectors, 
<<>>=
pupae <- read.csv("pupae.csv")
weight <- pupae$PupalWeight
gender <- pupae$Gender
weight.male <- weight[gender==0]
weight.female <- weight[gender==1]

# We will assume equal variance for male and female pupae (see Unequal variances, below):
t.test(weight.male, weight.female, var.equal=TRUE)
@

\begin{trybox}
Confirm that there are missing data in both variables in the example above. The default action is to omit all missing values (see description under \code{na.action} in the help file \code{?t.test}).
\end{trybox}

There is also a \emph{formula} interface for \code{t.test}. The formula interface is
important because we will use it in many other functions, like linear regression and linear modelling. For the \code{t.test} we can use the formula interface on the extracted variables, or without extracting the variables.

<<>>=
# Using the vectors we constructed in the previous example
t.test(weight ~ gender,  var.equal=TRUE)

# Or by specifying the data= argument.
t.test(PupalWeight~Gender,  data=pupae, var.equal=TRUE)
@


\subsection*{Paired data}

The \fun{t.test} can also be used when the data are paired, for example, measurements taken 
before and after some treatment on the same subjects. The pulse dataset is an example of paired data (see Section~\ref{sec:pulsedata}). We will compare pulse rates before and after exercise, including only those subjects that exercised (\code{Ran=1}),

<<>>=
pulse <- read.table("ms212.txt", header=TRUE)
pulse.before <- with(pulse, Pulse1[Ran==1])
pulse.after <- with(pulse, Pulse2[Ran==1])
t.test(pulse.after, pulse.before, paired=TRUE)
@

\subsection*{Unequal variances}

The default for the two-sample \fun{t.test} is actually to \emph{not} assume equal variances. The theory for this kind of test is quite complex, and the resulting $t$-test is now only approximate, with an adjustment called the 'Satterthwaite' or 'Welch' approximation made to the degrees of freedom.

<<>>=
t.test(PupalWeight ~ Gender,  data=pupae)
@

Since this modified $t$-test makes fewer assumptions, you could ask why we ever use the equal variances form. If the assumption is reasonable, then this (equal variances) form will have more power, i.e. will reject the null hypothesis more often when it is actually false.

\subsection*{Assumed normality}

The two-sample $t$-test assumes normality of the data (which you can check using a histogram or a QQ-plot) or that the sample sizes are large enough that the \emph{central limit theorem} applies. Note that the paired $t$-test assumes only that the differences are normal. The \fun{wilcox.test} can be used when any of these assumptions are suspect. In the case of two samples (unpaired), this test used is called the Wilcoxon rank sum test (also known as the Mann-Whitney test).

<<>>=
wilcox.test(pulse.after, pulse.before, paired=TRUE, exact=FALSE)
wilcox.test(PupalWeight ~ Gender,  data=pupae, exact=FALSE)
@

\subsection{Power}
\label{sec:power}

When testing a hypothesis, remember that there are two types of possible errors, due to the random nature of sampling data. These are the "Type 1 error" (rejecting the null hypothesis when it is actually true), and the "Type 2 error" (failing to reject the null when it is actually false). The probability of a Type 1 error is controlled by $\alpha$, the threshold on the $p$-value. The $p$-value is the probability of observing the test statistic, if the null hypothesis is actually true. So by keeping $\alpha$ small (for some reason, 0.05 is most commonly used), we control the chance of a Type 1 error. 

Statistical power is defined as 1 - the probability of a Type 2 error. Or in other words, the probability that we reject the null hypothesis when it is actually false. Consider the situation where we compare the means of two samples. It is easy to see that our power depends not only on $\alpha$, but also on the actual difference in means of the populations that the samples were drawn from. If they are very different, it will be easier to find a significant difference. So, to calculate the power we must specify how different the means are under the alternative hypothesis.

For a $t$-test, we can use the \fun{power.t.test} function to calculate the power. To approximate the power for the pupal weight $t$-test (as we saw in the previous section), we can use the following,

<<>>=
power.t.test(n=35, delta=0.08, sd=0.05, sig.level=0.05)
@

Here we have assumed equal groups of size 35 for each gender (although this is not exactly correct), a true difference in mean weights of 0.08, and a standard deviation of 0.05. The power is over 99\%, meaning that, with these conditions, we will be able to reject the null hypothesis 99\% of the time.

We can also calculate the required sample size, if we wish to attain a certain power. For example, suppose we want to detect a difference of 0.02 with 75\% power. What sample size do we need?

<<>>=
power.t.test(delta=0.02, sd=0.05, sig.level=0.05, power=0.75)
@

We would need 88 observations for each gender.


\begin{trybox}
Using \code{power.t.test} as in the examples above, see what happens when you set $\alpha$ (\code{sig.level}) to 0.01 or 0.1. Decide for yourself if the result makes sense.
\end{trybox}


\section{Simple linear regression}
\label{sec:simpleregression}

To fit linear models of varying complexity, we can use the \fun{lm} function. The simplest model is a straight-line relationship between an $x$ and a $y$ variable. In this situation, the assumption is that the $y$-variable (the response) is a linear function of the $x$-variable (the independent variable), plus some random noise or measurement error. For the simplest case, both $x$ and $y$ are assumed to be continuous variables. In statistical notation we write this as,
\begin{equation}
y = \alpha+\beta x +\varepsilon \label{eqn:simplelin}
\end{equation}

Here $\alpha$ and $\beta$ are (population) parameters that need to be estimated from the data. The error ($\epsilon$) is assumed to follow a normal distribution with a mean of zero, and a standard deviation of $\sigma$. It is also assumed that $\sigma$ is constant and does not depend on $x$.

Let's look at an example using the allometry data (see Fig.~\ref{fig:allomquickplot}),

<<allomquickplot, fig.cap='Quick inspection of the allometry data, before we perform a linear regression.', opts.label="smallsquare">>=
# Read data
allom <- read.csv("Allometry.csv")
plot(leafarea~diameter, data=allom)
@

We can see from this plot that leaf area generally increases with tree diameter. So we can use \code{lm}
to estimate the parameters in equation~\ref{eqn:simplelin}, or in other words to 'fit the model'.

<<>>=
# Fit linear regression of 'leafarea' on 'diameter',
# Results are stored in an object called model
model <- lm(leafarea~diameter, data=allom)

# Print a summary of the regression:
summary(model)

# Or just the coefficients (intercept and slope):
coef(model)
@

As you can see, \fun{lm} uses the formula interface that we discussed earlier (it always has the form y $\sim$ x). 

The \fun{summary} function prints a lot of information about the fit. In this case, it shows that the intercept is -\Sexpr{round(coef(model)[1],3)*-1}, which is the predicted leaf area for a tree with diameter of zero (not very useful in this case). 

It also shows a standard error and a $t$-statistic for this intercept, along with a p-value which shows that the intercept is significantly different from zero. The second line in the coefficients table shows the slope is \Sexpr{round(coef(model)[2],3)}, and that this is highly significantly different from zero.

In addition, we have a \code{Residual standard error} of \Sexpr{round(summary(model)$sigma,2)}, which is an estimate of $\sigma$, and an \code{R-squared} of \Sexpr{round(summary(model)$r.squared,2)} (which is the squared correlation coefficient). Finally, the \code{F-statistic} says whether the overall fit is significant, which in this case, is the same as the test for $\beta$ (because in this situation, the $F$-statistic is simply the square of the $t$-statistic).


It is straightforward to add the regression line to an existing plot (Fig.~\ref{fig:allomquickplot2}). Simply use \fun{abline} and the \code{model} object we created previously:
<<allomquickplot2, fig.cap='The allometry data, with an added regression line.', opts.label="smallsquare">>=
plot(leafarea~diameter, data=allom)
abline(model)
@

\subsection{Diagnostic plots}
\label{subsec:diagplots}

There are many ways to examine how well a model fits the data, and this step is important in deciding whether the model is appropriate. Most diagnostics are based on the residuals, the difference between the $\hat{y}=\hat{\alpha}+\hat{\beta} x$ fitted values and the actual data points.

If needed, the fitted values and residuals can be extracted using \code{fitted(model)} and \code{residuals(model)} respectively.

The two simplest and most useful diagnostic plots are the scale-location plot and a QQ-plot of the residuals. These can be produced with \code{plot}, but we much prefer two functions from the \pack{car} package, as shown by the following example (Fig.~\ref{fig:diagnos1}):
<<diagnos1, fig.cap='Two standard diagnostic plots for a fitted lm object.', opts.label='wide', echo=-c(1:2)>>=
palette("default")
par(mfrow=c(1,2))
model <- lm(leafarea ~ diameter, data=allom)

library(car)
residualPlot(model)
qqPlot(model)
@

The scale-location plot shows  the square root of the \emph{standardized} residuals against the fitted values. In an ideal situation, there should be no structure in this plot. Any curvature indicates that the model is under- or over-fitting, and a general spread-out (or contracting) from left to right indicates non-constant variance ('heteroscedasticity'). The QQ-plot enables us to check for departures from normality. Ideally, the standardized residuals should lie on a straight line.

Some departure from the straight line is to be expected though, even when the underlying distribution is really normal. The \fun{qqPlot} function from the \pack{car} package enhances the standard QQ-plot, by including a confidence interval (Fig~\ref{fig:qqplotcar}). In this case, there is some evidence of heteroscedasticity, and possibly curvature. 

The following code makes the QQ-plot and a plot of the data on a log-log scale (Fig.~\ref{fig:qqplotcar}).

<<qqplotcar, fig.cap='A plot of the Allometry data on a log-log scale.', opts.label="wide", echo=-c(1:2)>>=
palette("default")
par(mfrow=c(1,2))
library(car)
qqPlot(model)
plot(leafarea ~ diameter, data=allom, log="xy")
@


On a log-log scale, it looks like the variance is much more constant, and the relationship is more linear. So, we go ahead and refit the model to log-transformed variables. 

As we can see in Fig.~\ref{fig:qqplotcar}, the diagnostic plots look much better, except for a couple of points at the lower left corner of the QQ-plot. Notice that these outliers have been marked with their row number from the dataframe.

The following code produces Fig.~\ref{fig:diagnos2}, including diagnostic plots and a plot of the data with a regression line added. Note that the \fun{abline} function will only work as intended (shown before) on the log-log plot if we use log to the base 10 (\fun{log10}), in the model fit.

<<diagnos2, fig.cap='Diagnostic plots for the allometry data, refitted on a log-log scale (left panels). The allometry data fitted on a log-log scale, with the regression line (right panel).', opts.label="extrawide", echo=-1>>=
par(mfrow=c(1,3))
model_log <- lm(log10(leafarea)~log10(diameter), data=allom)
summary(model_log)

residualPlot(model_log)
qqPlot(model_log)

plot(leafarea~diameter, data=allom, log="xy")
abline(model_log)
@


\begin{trybox}
The residuals of a linear model fit can be extracted with the \fun{residuals} function. For one of the linear models from the above examples, extract the residuals, and make a histogram to help inspect normality of the residuals.
\end{trybox}



\clearpage
% Table of functions ------------------------------------------------------------
\section{Functions used in this chapter}
\label{sec:funtabbasic}

For functions not listed here, please refer to the index at the end of this book.

\begin{functionstable}
\fun{abline} & Adds a straight line to an existing plot. For a vertical line, use \code{v} and an x value. For a horizontal line, use \code{h} and a y value. To add a sloped line, give a slope (\code{a}), and an intercept (\code{b}). Can also be passed a linear model object, see Section~\ref{sec:simpleregression}. & \makecell{abline(v=5)}\\
%
\fun{curve} & Can be used to plot curves, or to add a curve to an existing plot. The first argument can be either a function or an expression that evaluates to a curve. To specify the range of the curve, use \code{from} and \code{to}. Use \code{add=TRUE} to add a curve to an existing plot. & \makecell{curve(cos(x),\\ \hskip 2em from=0, to=2*pi)}\\
%
\fun{kurtosis} & From the \pack{moments} package. Computes Pearson's measure of kurtosis. & \\
%
\fun{length} & Returns the total number of elements in its input. & length(LETTERS) \\
%
\fun{lm} & Fits linear models to data. Requires a formula in the form y $\sim$ x. Also possible to specify \code{data} to be used and an \code{na.action}. Returns a linear model object, which can be used for further analysis. & See Section~\ref{sec:simpleregression}\\
%
\fun{log} & Computes natural logarithms (base $e$) of its input. For common (base 10) logarithms, use \fun{log10}. & log(3)\\
%
\fun{mean} & Returns the arithmetic average of its input. & mean(c(1,2,3,10))\\
%
\fun{power.t.test} & Computes the probability that a given $t$-test will accept the null hypothesis even though it is false. See Section~\ref{sec:power}. & \makecell{power.t.test(n=35,\\ \hskip 2em delta=0.08,\\ \hskip 2em sd=0.05,\\ \hskip 2em sig.level=0.05)}\\
%
\fun{prop.test} & Tests whether an experimental proportion is equal to a hypothesized proportion. Takes \code{x} (number of successes), \code{n} (total number of trials) and \code{p} (the hypothesized proportion.) Specify what kind of test to use (\code{"two.sided", "less", "greater"}) using \code{alternative}.  & \makecell{prop.test(x=60,\\ \hskip 2em n=100,\\ \hskip 2em p=0.5)}\\
%
\fun{qqPlot} & From the \pack{car} package. Given a linear model object, creates a prettified quantile-quantile plot. & See Section~\ref{subsec:diagplots}\\
%
\fun{quantile} & Computes the quantiles of given data. Defaults are quartiles, but other sequences can be supplied using \code{probs}.  & See Section~\ref{sec:descstat}\\
%
\fun{residuals} & Extracts residuals from a linear model object. & \\
%
\fun{sd} & Returns the standard deviation of its input. & sd(c(99,85,50,87,89)) \\
%
\fun{skewness} & From the \pack{moments} package. Computes the skewness of a sample. & \\
%
\fun{summary} & Provides a summary of the variables in a dataframe. Statistics are given for numerical variables, and counts are given for factor variables. & summary(warpbreaks) \\
%
\fun{t.test} & Computes Student's $t$-test for a given sample of data. It is possible to compare two samples, or a single sample to a hypothesized mean (\code{mu}). & See Section~\ref{sec:inference}\\
%
\fun{tapply} & Used to summarize data in dataframes. Will be discussed further in Chapter~\ref{chap:summarize}. & See Section~\ref{sec:tapply}\\
%
\fun{var} & Returns the variance of its input. & var(c(99,85,50,87,89))\\
%
\fun{wilcox.test} & Computes the Wilcoxon test, a nonparametric alternative to $t$-tests for small or non-normal samples. Can be used to compare two samples, or a single sample to a hypothesized mean (\code{mu}). & See Section~\ref{sec:inference}\\
%
\end{functionstable}


\newpage
\section{Exercises}

<<chap5exercises, child="05_basicstats_Exercises.Rnw", eval=TRUE, echo=FALSE, results='hide', fig.keep='none',warning=FALSE>>=

@

