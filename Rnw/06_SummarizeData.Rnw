%---------------------------------------------------------------------------%
% Copyright 2015 Remko Duursma, Jeff Powell                                 %
%                                                                           %
% This file is part of HIERmanual                                           %
%                                                                           %
%     HIERmanual is free software: you can redistribute it and/or modify    %
%     it under the terms of the GNU General Public License as published by  %
%     the Free Software Foundation, either version 3 of the License, or     %
%     (at your option) any later version.                                   %
%                                                                           % 
%     HIERmanual is distributed in the hope that it will be useful,         %
%     but WITHOUT ANY WARRANTY; without even the implied warranty of        %
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         %
%     GNU General Public License for more details.                          %
%                                                                           %
%     You should have received a copy of the GNU General Public License     %
%     along with HIERmanual.  If not, see <http://www.gnu.org/licenses/>.   %
%---------------------------------------------------------------------------%

\chapter[Summarizing, tabulating and merging data]{Summarizing, tabulating and\\ merging data}
\label{chap:summarize}


\section{Summarizing dataframes}

There are a few useful functions to print general summaries of a dataframe, to see which variables are included, what types of data they contain, and so on. We already looked at some of these in Section~\ref{sec:dataframes}.

The most basic function is \FUN{summary}, which works on many types of objects. 

Let's look at the output for the \code{allom} dataset.
<<echo=FALSE>>=
allom <- read.csv("Allometry.csv")
@
<<>>=
summary(allom)
@

For each factor variable, the levels are printed (the \code{species} variable, levels \code{PIMO}, \code{PIPO} and \code{PSME}. For all numeric variables, the minimum, first quantile, median, mean, third quantile, and the maximum values are shown.

To simply see what types of variables your dataframe contains (or, for objects other than dataframes, to summarize sort of object you have), use the \fun{str} function (short for 'structure').

<<>>=
str(allom)
@

Finally, there are two very useful functions in the \pack{Hmisc} package (recall how to install and load packages from Section~\ref{sec:packages}). The first, \fun{describe}, is much like \fun{summary}, but offers slightly more sophisticated statistics.

The second, \fun{contents}, is similar to \fun{str}, but does a very nice job of summarizing the \code{factor} variables in your dataframe, prints the number of missing variables, the number of rows, and so on. 

<<echo=FALSE>>=
suppressPackageStartupMessages(library(Hmisc))
@
<<>>=
# read data
pupae <- read.csv("pupae.csv")

# Make sure CO2_treatment is a factor (it will be read as a number)
pupae$CO2_treatment <- as.factor(pupae$CO2_treatment)

# Show contents:
library(Hmisc)  
contents(pupae)
@

Here, \code{storage} refers to the internal storage type of the variable: note that the factor variables are stored as 'integer', and other numbers as 'double' (this refers to the precision of the number).


\begin{trybox}
Use the \code{describe} function from the \code{Hmisc} package on the allometry data, and compare the output to \code{summary}.
\end{trybox}


\section{Making summary tables}
\label{sec:tapplyaggregate}

\subsection{Summarizing vectors with \code{tapply()}}
\label{sec:tapply}

% One-dimensional tapply box.
\fbox{
\begin{minipage}[c]{0.98\textwidth}
If we have the following dataset called \code{plantdat},

\includegraphics[width=0.35\textwidth]{screenshots/exampledata}

and execute the command

<<eval=FALSE>>=
with(plantdat, tapply(Plantbiomass, Treatment, mean))
@

we get the result

\includegraphics[width=0.35\textwidth]{screenshots/tapplyresult}

Note that the result is a \code{vector} (elements of a vector can have names, like columns of a dataframe).

\end{minipage}}



% Two-dimensional tapply box.
\fbox{
\begin{minipage}[c]{0.98\textwidth}
If we have the following dataset called \code{plantdat2},

\includegraphics[width=0.4\textwidth]{screenshots/exampledatalarger}

and execute the command

<<eval=FALSE>>=
with(plantdat2, tapply(Plantbiomass, list(Species, Treatment), mean))
@

we get the result

\includegraphics[width=0.4\textwidth]{screenshots/tapplyresultlarger}

Note that the result here is a \code{matrix}, where \code{A} and \code{B}, the species codes, are the rownames of this matrix.

\end{minipage}}



Often, you want to summarize a variable by the levels of another variable. For example, in the \code{rain} data (see Section~\ref{sec:raindata}), the \code{Rain} variable gives daily values, but we might want to calculate annual sums,

<<>>=
# Read data
rain <- read.csv("Rain.csv")

# Annual rain totals.
with(rain, tapply(Rain, Year, FUN=sum))
@

The \FUN{tapply} function applies a function (\code{sum}) to a vector (\code{Rain}), that is split into chunks depending on another variable (\code{Year}). 

We can also use the \fun{tapply} function on more than one variable at a time. Consider these examples on the \code{pupae} data. 
 
<<>>=
# Read data
pupae <- read.csv("pupae.csv")

# Average pupal weight by CO2 and T treatment:
with(pupae, tapply(PupalWeight, list(CO2_treatment, T_treatment), FUN=mean))

# Further split the averages, by gender of the pupae.
with(pupae, tapply(PupalWeight, list(CO2_treatment, T_treatment, Gender), FUN=mean))
@

As the examples show, the \fun{tapply} function produces summary tables by one or more factors. The resulting object is either a vector (when using one factor), or a matrix (as in the examples using the pupae data). 

The limitations of \fun{tapply} are that you can only summarize one variable at a time, and that the result is not a dataframe.

The main advantage of \code{tapply} is that we can use it as input to \fun{barplot}, as the following example demonstrates (Fig.~\ref{fig:pupgroupedbar})

<<pupgroupedbar, fig.cap="A grouped barplot of average pupal weight by CO2 and Gender for the pupae dataset. This is easily achieved via the use of tapply.", opts.label="smallsquare">>=
# Pupal weight by CO2 and Gender. Result is a matrix.
pupm <- with(pupae, tapply(PupalWeight, list(CO2_treatment,Gender), 
                           mean, na.rm=TRUE))

# When barplot is provided a matrix, it makes a grouped barplot.
# We specify xlim to make some room for the legend.
barplot(pupm, beside=TRUE, legend.text=TRUE, xlim=c(0,8),
        xlab="Gender", ylab="Pupal weight")
@



\subsection{Summarizing dataframes with \code{summaryBy}}
\label{sec:summaryby}

% One-dimensional summaryBy box.
\fbox{
\begin{minipage}[c]{0.98\textwidth}
If we have the following dataset called \code{plantdat},

\includegraphics[width=0.3\textwidth]{screenshots/exampledata}

and execute the command

<<eval=FALSE>>=
library(doBy)

summaryBy(Plantbiomass ~ treatment, FUN=mean, data=plantdat)
@

we get the result

\includegraphics[width=0.3\textwidth]{screenshots/summarybyresult}

Note that the result here is a \code{dataframe}.

\end{minipage}}



% Two-dimensional summaryBy box.
\fbox{
\begin{minipage}[c]{0.98\textwidth}
If we have the following dataset called \code{plantdat2},

\includegraphics[width=0.4\textwidth]{screenshots/exampledatalarger}

and execute the command

<<eval=FALSE>>=
summaryBy(Plantbiomass ~ Species + Treatment, FUN=mean, data=dfr)
@

we get the result

\includegraphics[width=0.4\textwidth]{screenshots/summarybyresultlarger}

Note that the result here is a \code{dataframe}.

\end{minipage}}


In practice, it is often useful to make summary tables of multiple variables at once, and to end up with a dataframe. In this book we use \FUN{summaryBy}, from the \PACK{doBy} package, to achieve this. (We ignore the \code{aggregate} function in base \R{}, because \code{summaryBy} is much easier to use).

With \fun{summaryBy}, we can generate multiple summaries (mean, standard deviation, etc.) on more than one variable in a dataframe at once. We can use a convenient formula interface for this. It is of the form,

\begin{verbatim}
summaryBy(Yvar1 + Yvar2 ~ Groupvar1 + Groupvar2, FUN=c(mean,sd), data=mydata)
\end{verbatim}

where we summarize the (numeric) variables \code{Yvar1} and \code{Yvar2} by all combinations of the (factor) variables \code{Groupvar1} and \code{Groupvar2}.

<<echo=FALSE>>=
suppressPackageStartupMessages(library(doBy))
@

<<>>=
# Load the doBy package
library(doBy)

# read pupae data if you have not already
pupae <- read.csv("pupae.csv")

# Get mean and standard deviation of Frass by CO2 and T treatments
summaryBy(Frass ~ CO2_treatment + T_treatment,
          data=pupae, FUN=c(mean,sd))

# Note that there is a missing value. We can specify na.rm=TRUE,
# which will be passed to both mean() and sd(). It works because those
# functions recognize that argument (i.e. na.rm is NOT an argument of 
# summaryBy itself!)
summaryBy(Frass ~ CO2_treatment + T_treatment,
          data=pupae, FUN=c(mean,sd), na.rm=TRUE)

# However, if we use a function that does not recognize it, we first have to
# exclude all missing values before making a summary table, like this:
pupae_nona <- pupae[complete.cases(pupae),]

# Get mean and standard deviation for
# the pupae data (Pupal weight and Frass), by CO2 and T treatment.
# Note that length() does not recognize na.rm (see ?length), which is
# why we have excluded any NA from pupae first.
summaryBy(PupalWeight+Frass ~ CO2_treatment + T_treatment,
          data=pupae_nona,
          FUN=c(mean,sd,length))
@

You can also use any function that returns a vector of results. In the following example we calculate the 5\% and 95\% quantiles of all numeric variables in the allometry dataset. To do this, use \code{.} for the left-hand side of the formula.

<<>>=
# . ~ species means 'all numeric variables by species'.
# Extra arguments to the function used (in this case quantile) can be set here as well,
# they will be passed to that function (see ?quantile).
summaryBy(. ~ species, data=allom, FUN=quantile, probs=c(0.05, 0.95))
@




\subsection*{Working example : Calculate daily means and totals}
\label{sec:dailymeansexample}

Let's look at a more advanced example using weather data collected at the Hawkesbury Forest Experiment in 2008 (see Section~\ref{sec:hfemet}). The data given are in half-hourly time steps. It is a reasonable request to provide data as daily averages (for temperature) and daily sums (for precipitation). 

The following code produces a daily weather dataset, and Fig.~\ref{fig:hfemetaggregate}.

<<hfemetaggregate,  fig.cap='Daily rainfall at the HFE in 2008', opts.label="smallsquare">>=
# Read data
hfemet <- read.csv("HFEmet2008.csv")

# This is a fairly large dataset:
nrow(hfemet)

# So let's only look at the first few rows:
head(hfemet)

# Read the date-time
library(lubridate)
hfemet$DateTime <- mdy_hm(hfemet$DateTime)

# Add a new variable 'Date', a daily date variable
hfemet$Date <- as.Date(hfemet$DateTime)

# First aggregate some of the variables into daily means:
library(doBy)
hfemetAgg <- summaryBy(PAR + VPD + Tair ~ Date, data=hfemet, FUN=mean)
# (look at head(hfemetAgg) to check this went OK!)

#--- Now get daily total Rainfall:
hfemetSums <- summaryBy(Rain ~ Date, data=hfemet, FUN=sum)

# To finish things off, let's make a plot of daily total rainfall:
# (type='h' makes a sort of narrow bar plot)
plot(Rain.sum ~ Date, data=hfemetSums, type='h', ylab=expression(Rain~(mm~day^-1)))
@



\subsection{Tables of counts}
\label{sec:xtabs}

It is often useful to count the number of observations by one or more multiple factors. One option is to use \fun{tapply} or \fun{summaryBy} in combination with the \fun{length} function. A much better alternative is to use the \FUN{xtabs} and \FUN{ftable} functions, in addition to the simple use of \fun{table}.

Consider these examples using the Titanic data (see Section~\ref{sec:titanic}).
<<tidy=FALSE>>=
# Read titanic data
titanic <- read.table("titanic.txt", header=TRUE)

# Count observations by passenger class
table(titanic$PClass)

# With more grouping variables, it is more convenient to use xtabs.
# Count observations by combinations of passenger class, sex, and whether they survived:
xtabs( ~ PClass + Sex + Survived, data=titanic)

# The previous output is hard to read, consider using ftable on the result:
ftable(xtabs( ~ PClass + Sex + Survived, data=titanic))
@

\subsection{Adding simple summary variables to dataframes}
\label{sec:summaryvars}

We saw how \code{tapply} can make simple tables of averages (or totals, or other functions) of some variable by the levels of one or more factor variables. The result of \fun{tapply} is typically a vector with a length equal to the number of levels of the factor you summarized by (see examples in Section~\ref{sec:tapply}). 

What if you want the result of a summary that is the length of the original vector? One option is to aggregate the dataframe with \code{summaryBy}, and \code{merge} it back to the original dataframe (see Section~\ref{sec:merge}). But we can use a shortcut.

Consider the \code{allometry} dataset, which includes tree height for three species. Suppose you want to add a new variable 'MaxHeight', that is the maximum tree height observed per species. We can use \FUN{ave} to achieve this:
<<>>=
# Read data
allom <- read.csv("Allometry.csv")

# Maximum tree height by species:
allom$MaxHeight <- ave(allom$height, allom$species, FUN=max)

# Look at first few rows (or just type allom to see whole dataset)
head(allom)
@
Note that you can use any function in place of \fun{max}, as long as that function can take a vector as an argument, and returns a single number. 

\begin{trybox}
If you want results similar to \code{ave}, you can use \fun{summaryBy} with the argument \code{full.dimension=TRUE}. Try \code{summaryBy} on the \code{pupae} dataset with that argument set, and compare the result to \code{full.dimension=FALSE}, which is the default.
\end{trybox}



\subsection{Reordering factor levels based on a summary variable}
\label{sec:reorder}

It is often useful to tabulate your data in a meaningful order. We saw that, when using \code{summaryBy}, \code{tapply} or similar functions, that the results are always in the order of your factor levels. Recall that the default order is alphabetical. This is rarely what you want.

You can reorder the factor levels by some summary variable. For example,

\index{reorder}
\label{page:reorder}
<<warning=FALSE>>=
# Reorder factor levels for 'Manufacturer' in the cereal data 
# by the mean amount of sodium.

# Read data, show default (alphabetical) levels:
cereal <- read.csv("cereals.csv")
levels(cereal$Manufacturer)

# Now reorder:
cereal$Manufacturer <- with(cereal, reorder(Manufacturer, sodium, median, na.rm=TRUE))
levels(cereal$Manufacturer)

# And tables are now printed in order:
with(cereal, tapply(sodium, Manufacturer, median))
@

This trick comes in handy when making barplots; it is customary to plot them in ascending order if there is no specific order to the factor levels, as in this example.

The following code produces Fig.~\ref{fig:coweetabar}.

<<echo=FALSE>>=
suppressPackageStartupMessages(library(gplots))
suppressPackageStartupMessages(library(doBy))
@

<<coweetabar, fig.cap='An ordered barplot for the coweeta tree data (error bars are 1 SD).', warning=FALSE, opts.label="largesquare">>=
coweeta <- read.csv("coweeta.csv")
coweeta$species <- with(coweeta, reorder(species, height, mean, na.rm=TRUE))

library(doBy)
coweeta_agg <- summaryBy(height ~ species, data=coweeta, FUN=c(mean,sd))

library(gplots)

# This par setting makes the x-axis labels vertical, so they don't overlap.
par(las=2)
with(coweeta_agg, barplot2(height.mean, names.arg=species,
                          space=0.3, col="red",plot.grid=TRUE,
                          ylab="Height (m)",
                          plot.ci=TRUE,
                          ci.l=height.mean - height.sd,
                          ci.u=height.mean + height.sd))
@

\begin{trybox}
The above example orders the factor levels by increasing median sodium levels. Try reversing the factor levels, using the following code after \code{reorder}.
\begin{verbatim}
coweeta$species <- factor(coweeta$species, levels=rev(levels(coweeta$species)))
\end{verbatim}
Here we used \fun{rev} to reverse the levels.
\end{trybox}



\section{Combining dataframes}


\subsection{Merging dataframes}
\label{sec:merge}

% merge box
\fbox{
\begin{minipage}[c]{0.98\textwidth}
If we have the following dataset called \code{plantdat},

\includegraphics[width=0.35\textwidth]{screenshots/exampledata}

and we have another dataset, that includes the same \code{PlantID} variable (but is not necessarily ordered, nor does it have to include values for every plant):

\includegraphics[width=0.25\textwidth]{screenshots/leafnitrogendata}

and execute the command

<<eval=FALSE>>=
merge(plantdat, leafnitrogendata, by="PlantID")
@

we get the result

\includegraphics[width=0.45\textwidth]{screenshots/mergeresult}

Note the missing value (\code{NA}) for the plant for which no leaf nitrogen data was available.

\end{minipage}}

\label{page:mergefun}
In many problems, you do not have a single dataset that contains all the measurements you are interested in -- unlike most of the example datasets in this tutorial. Suppose you have two datasets that you would like to combine, or \FUN{merge}. This is straightforward in \R{}, but there are some pitfalls.

Let's start with a common situation when you need to combine two datasets that have a different number of rows. 
<<>>=
# Two dataframes
data1 <- data.frame(unit=c("x","x","x","y","z","z"),Time=c(1,2,3,1,1,2))
data2 <- data.frame(unit=c("y","z","x"), height=c(3.4,5.6,1.2))

# Look at the dataframes
data1
data2

# Merge dataframes:
combdata <- merge(data1, data2, by="unit")

# Combined data
combdata
@

Sometimes, the variable you are merging with has a different name in either dataframe. In that case, you can either rename the variable before merging, or use the following option:

\begin{verbatim}
merge(data1, data2, by.x="unit", by.y="item")
\end{verbatim}

Where \code{data1} has a variable called 'unit', and \code{data2} has a variable called 'item'.

Other times you need to merge two dataframes with multiple key variables. Consider this example, where two dataframes have measurements on the same units at some of the the same times, but on different variables:

<<>>= 
# Two dataframes
data1 <- data.frame(unit=c("x","x","x","y","y","y","z","z","z"),
Time=c(1,2,3,1,2,3,1,2,3),
Weight=c(3.1,5.2,6.9,2.2,5.1,7.5,3.5,6.1,8.0))
data2 <- data.frame(unit=c("x","x","y","y","z","z"),
Time=c(1,2,2,3,1,3),
Height=c(12.1,24.4,18.0,30.8,10.4,32.9))

# Look at the dataframes
data1
data2

# Merge dataframes:
combdata <- merge(data1, data2, by=c("unit","Time"))

# By default, only those times appear in the dataset that have measurements
# for both Weight (data1) and Height (data2)
combdata

# To include all data, use this command. This produces missing values for some times:
merge(data1, data2, by=c("unit","Time"), all=TRUE)
# Compare this result with 'combdata' above!
@



\subsection*{Merging multiple datasets}
\label{sec:multmerge}

% How I made cereal1, cereal2, cereal3
% i <- sample(1:77,10)
% cereal1 <- cereal[i,c("Cereal.name","protein")]
% cereal2 <- cereal[i,c("Cereal.name","vitamins")]
% cereal3 <- cereal[i,c("Cereal.name","sugars")]
% 
% cereal2 <- cereal2[-c(1,9),]
% cereal3 <- cereal3[-c(3,4,5,6),]
% 
% names(cereal2)[1] <- "cerealbrand"
% names(cereal3)[1] <- "cerealname"
% 
% write.csv(cereal1, "cereal1.csv", row.names=FALSE)
% write.csv(cereal2, "cereal2.csv", row.names=FALSE)
% write.csv(cereal3, "cereal3.csv", row.names=FALSE)

Consider the cereal dataset (Section~\ref{sec:cerealdata}), which gives measurements of all sorts of contents of cereals. Suppose the measurements for 'protein', 'vitamins' and 'sugars' were all produced by different labs, and each lab sends you a separate dataset.
To make things worse, some measurements for sugars and vitamins are missing, because samples were lost in those labs. 

How to put things together?
<<>>=
# Read the three datasets given to you from the three different labs:
cereal1 <- read.csv("cereal1.csv")
cereal2 <- read.csv("cereal2.csv")
cereal3 <- read.csv("cereal3.csv")

# Look at the datasets:
cereal1
cereal2
cereal3

# Note that the number of rows is different between the datasets, 
# and even the index name ('Cereal.name') differs between the datasets.

# To merge them all together, use merge() twice, like this.
cerealdata <- merge(cereal1, cereal2, 
                    by.x="Cereal.name", 
                    by.y="cerealbrand", all.x=TRUE)
# NOTE: all.x=TRUE specifies to keep all rows in cereal1 that do not exist in cereal2.

# Then merge again:
cerealdata <- merge(cerealdata, cereal3, 
                    by.x="Cereal.name", 
                    by.y="cerealname", all.x=TRUE)

# And double check the final result
cerealdata
# Note that missing values (NA) have been inserted where some data was not available.
@
\index{merge}


\subsection{Row-binding dataframes}
\label{sec:rbind}

% rbind box
\fbox{
\begin{minipage}[c]{0.98\textwidth}
If we have the following dataset called \code{plantdat},

\includegraphics[width=0.35\textwidth]{screenshots/rbindinput}

and we have another dataset (\code{plantdatmore}), \emph{with exactly the same columns} (including the names and order of the columns),

\includegraphics[width=0.4\textwidth]{screenshots/moredataforrbind}

and execute the command

<<eval=FALSE>>=
rbind(plantdat, plantdatmore)
@

we get the result

\includegraphics[width=0.4\textwidth]{screenshots/rbindresult}

\end{minipage}}


Using \fun{merge}, we are able to glue dataframes together side-by-side based on one or more 'index' variables. Sometimes you have multiple datasets that can be glued together top-to-bottom, for example when you have multiple very similar dataframes. We can use the \FUN{rbind} function, like so:
<<>>=
# Some fake data
mydata1 <- data.frame(var1=1:3, var2=5:7) 
mydata2 <- data.frame(var1=4:6, var2=8:10) 

# The dataframes have the same column names, in the same order:
mydata1
mydata2

# So we can use rbind to row-bind them together:
rbind(mydata1, mydata2)
@

Let's look at the above \fun{rbind} example again but with a modification where some observations are duplicated between dataframes. This might happen, for example, when working with files containing time-series data and where there is some overlap between the two datasets. The \fun{union} function from the \pack{dplyr} package only returns unique observations:

<<echo=FALSE>>=
suppressPackageStartupMessages(library(dplyr))
@
<<>>=
# Some fake data
mydata1 <- data.frame(var1=1:3, var2=5:7) 
mydata2 <- data.frame(var1=2:4, var2=6:8) 

# The dataframes have the same column names, in the same order:
mydata1
mydata2

# 'rbind' leads to duplicate observations, 'union' removes these:
library(dplyr)
union(mydata1, mydata2)
rbind(mydata1, mydata2)
@


Sometimes, you want to \code{rbind} dataframes together but the column names do not exactly match. One option is to first process the dataframes so that they do match (using subscripting). Or, just use the \fun{bind\_rows} function from the \pack{dplyr} package. Look at this example where we have two dataframes that have only one column in common, but we want to keep all the columns (and fill with NA where necessary),
<<>>=
# Some fake data
mydata1 <- data.frame(index=c("A","B","C"), var1=5:7) 
mydata2 <- data.frame(var1=8:10, species=c("one","two","three")) 

# smartbind the dataframes together
bind_rows(mydata1, mydata2)
@

\emph{Note:} an equivalent function to bind dataframes side-by-side is \fun{cbind}, which can be used instead of \code{merge} when no index variables are present. However, in this book, the use of \code{cbind} is discouraged for dataframes (and we don't discuss matrices), as it can lead to problems that are difficult to fix. 

\begin{trybox}
The \code{dplyr} package contains a number of functions for merging dataframes that may be more intuitive to you than \code{merge}. For example, \fun{left\_join(mydata1, mydata2)} keeps all rows from \code{mydata1} after merging the two dataframes, while \fun{right\_join(mydata1, mydata2)} keeps all rows from \code{mydata2}. Read more about the \code{dplyr} functions for joining tables at ?\code{dplyr::join}, and try to recreate the examples above using these functions instead of \code{merge}.
\end{trybox}



\section{Exporting summary tables}

To export summary tables generated with \code{aggregate}, \code{tapply} or \code{table} to text files, you can use \fun{write.csv} or \fun{write.table} just like you do for exporting dataframes (see Section~\ref{sec:writecsv}).

For example,

<<eval=FALSE>>=
cereals <- read.csv("cereals.csv")

# Save a table as an object
mytable <- with(cereals, table(Manufacturer, Cold.or.Hot))

# Write to file
write.csv(mytable, "cerealtable.csv")
@

\subsection{Inserting tables into documents using R markdown}
\label{sec:markdowntablesword}
\index{R markdown!tables in}


How do we insert tables from \R{} into Microsoft Word (for example, results from \code{tapply} or \code{aggregate})? The best way is, of course, to use R markdown. There are now several packages that make it easy to produce tables in markdown, and from there, markdown easily converts them into Word. First, we'll need some suitable data. We'll start by making a summary table of the pupae dataset we loaded earlier in the chapter. 

<<eval=FALSE>>=
# Load the doBy package
library(doBy)

# read pupae data if you have not already
pupae <- read.csv("pupae.csv")

# Make a table of means and SD of the pupae data
puptab <- summaryBy(Frass + PupalWeight ~ CO2_treatment + T_treatment,
                         FUN=c(mean,sd), data=pupae, na.rm=TRUE)

# It is more convenient to reorder the dataframe to have sd and mean
# together.
puptab <- puptab[,c("CO2_treatment","T_treatment",
                    "Frass.mean","Frass.sd",
                    "PupalWeight.mean","PupalWeight.sd")]

# Give the columns short, easy to type names
names(puptab) <- c("CO2","T","Frass","SD.1","PupalWeight","SD.2")

# Convert temperature, which is a factor, to a character variable
# (Factors don't work well with the data-reshaping that takes place in pixiedust)
puptab$T <- as.character(puptab$T)
@

To insert any of the following code into a markdown document, use the following around each code chunk:

<<eval=FALSE>>=
```{r echo=TRUE, results="asis"}
```
@

\subsubsection*{Tables with kable}
\label{sec:tableswkable}
\index{R markdown!kable function}

We'll start with the \FUN{kable} function from the \PACK{knitr} package. The \pack{knitr} package provides the functionality behind RStudio's markdown interface, so, if you have been using markdown, you don't need to install anything extra to use \fun{kable}. This function is simple, robust and offers the ability to easily add captions in Word.

<<eval=FALSE>>=
library(knitr)
kable(puptab, caption="Table 1. Summary stats for the pupae data.")
@

If you run this in the console, you'll see a markdown table. Knitting your markdown document as "Word" will result in a Word document with a table and a nice caption. You can rename the table columns using the \code{col.names} argument (just be sure to get them in the same order!)  

As you'll see in the next example, you can also use markdown formatting within the table -- note the addition of a subscript to CO\textsubscript{2}.

<<eval=FALSE>>=
kable(puptab, caption="Table 1. Summary stats for the pupae data.", 
      col.names=c("CO~2~ Treatment","Temperature","Frass","SD","Pupal Weight", "SD"))
@

Other things that are easily controlled using \fun{kable} are the number of digits shown in numeric columns (\code{digits}), row names (\code{row.names}), and the alignment of each column (\code{align}).

<<eval=FALSE>>=
kable(puptab, caption="Table 1. Summary stats for the pupae data.", 
      col.names=c("CO~2~ Treatment","Temperature","Frass","SD","Pupal Weight", "SD"),
      digits=1,
      align=c('l','c','r')) # Values will be recycled to match number of columns
@


\subsubsection*{Tables with pander}
\label{sec:tableswpander}
\index{R markdown!pander package}

Other options for creating tables in markdown include the function \FUN{pander} from the \PACK{pander} package. This function is designed to translate a wide range of input into markdown format. When you give it input it can recognize as a table, it will output a markdown table. Like \fun{kable}, it can take a caption argument.

<<eval=FALSE>>=
library(pander)
pander(puptab, caption = "Table 1. Summary stats for the pupae data")
@

One of the advantages of \fun{pander} is that it can accept a broader range of input than \fun{kable}. For instance, it can  make tables out of the results produced by linear models (something we'll discuss in Chapter~\ref{chap:linmodel}.) Formatting options available in \fun{pander} include options for rounding, cell alignment (including dynamic alignments based on cell values), and adding emphasis, such as italic or bold formatting (which can also be set dynamically). 

<<eval=FALSE>>=
library(pander)

# Cell and row emphasis are set before calling pander,
# using functions that start with 'emphasize.':
# emphasize.strong.rows,  emphasize.strong.cols,  emphasize.strong.cells,
# emphasize.italics.rows, emphasize.italics.cols, emphasize.italics.cells
emphasize.strong.cols(1)
emphasize.italics.cells(which(puptab == "elevated", arr.ind = TRUE))
pander(puptab, 
       caption = "Table 1. Summary stats for the pupae data, with cell formatting",
       # for <justify>, length must match number of columns
       justify = c('left', 'center', 'right','right','right','right'),
       round=3)
@

There are also options to set what type of table you would like (e.g., \code{grid}, \code{simple}, or \code{multiline}), cell width, line breaking within cells, hyphenation, what kind of decimal markers to use, and how replace missing values. For more details on these, see the pander vignettes, or \code{?pandoc.table.return}. 



\subsubsection*{Tables with pixiedust}
\label{sec:tableswpdust}
\index{R markdown!pixiedust package}


A third option for formatting tables is the imaginatively-named \PACK{pixiedust} package. The syntax used by \pack{pixiedust} is a bit different the other packages we've discussed so far. It starts with the function \FUN{dust}, then sends the results of addition options (called 'sprinkles') to \fun{dust} using the symbols \code{\%>\%}, which are known as the pipe operator. To make a markdown table, use the 'markdown' sprinkle:

<<eval=FALSE>>=
# pixiedust requires broom for table formatting
library(broom)
library(pixiedust)

dust(puptab) %>%
  sprinkle_print_method("markdown")
@

There are a range of formatting options similar to those available in \fun{pander}, including bold, italic, a missing data string, and rounding. Each of these is added as a new sprinkle. Cells are specified by row and column:

<<eval=FALSE>>=
dust(puptab) %>%
  sprinkle_print_method("markdown")  %>%
  sprinkle(rows = c(2, 4), bold = TRUE) %>%
  sprinkle(rows = c(3, 4), cols=c(1, 1), italic = TRUE) %>%
  sprinkle(round = 1)
@

It is also possible to add captions, change column names, and replace the contents of a given cell during formatting.

<<eval=FALSE>>=
# Captions are added to the dust() function
dust(puptab, caption="Table 1. Summary stats for the pupae data,
     formatted with pixiedust") %>%
  # Note that identical column names are not allowed
  sprinkle_colnames("CO2 Treatment","Temperature","Frass","Frass SD",
                    "Pupal Weight", "Weight SD") %>%
  # Replacement must have the same length as what it replaces
  sprinkle(cols = 1, replace = 
             c("ambient", "ambient", "elevated", "elevated")) %>%
  sprinkle_print_method("markdown")
@

If you use latex or html, you will want to look into this package further, as it has a number of special 'sprinkles' for these formats.


\begin{furtherreading}
If you want to know more about \fun{kable}, the help page has a thorough explanation. Type \code{?kable} at the command line.  There is a good introduction to \fun{pander} called "Rendering tables with pandoc.table" at \url{https://cran.r-project.org/web/packages/pander/vignettes/pandoc_table.html}. (Don't be confused by the name; \fun{pandoc.table} is the table function hidden under the hood of \fun{pander}.) Likewise, \pack{pixiedust} has a good introductory vignette called "Creating magic with pixiedust" at \url{https://cran.r-project.org/web/packages/pixiedust/vignettes/pixiedust.html}. For a list of all of the \pack{pixiedust} 'sprinkles' available, see \url{https://cran.r-project.org/web/packages/pixiedust/vignettes/sprinkles.html}.
\end{furtherreading}
% 
% \clearpage
% 
% % Table of functions ------------------------------------------------------------
% \section{Functions used in this chapter}
% \label{sec:funtabsummarize}
% 
% For functions not listed here, please refer to the index at the end of this book.  
% 
% \begin{functionstable}
% \fun{ave} & An easy way to add summary variables to a dataset.  Summarizes a vector (perhaps stored in a dataframe) by levels of a factor. Supply a numeric variable, a grouping factor, and a function to apply. Output is a vector with the same length as the original data. See Section~\ref{sec:summaryvars}. & \makecell{with(iris, ave(\\ \hskip 2em Sepal.Length, \\ \hskip 2em Species,\\ \hskip 2em FUN=mean))}\\
% % 
% \fun{cbind} & Combine two or more dataframes or matrices by binding the columns together. Must have equal number of rows in each. & cbind(pupae, pupae)  \\
% %
% \fun{contents} & From the \pack{Hmisc} package. Overview of types of variables included in a dataframe. & contents(iris)\\
% 
% \fun{data.frame} & Used to create a new dataframe from vectors or an existing dataframe. See Chapter~\ref{chap:readdata}. & \makecell{dfr <- data.frame(\\ \hskip 2em a=-5:0, b=10:15)}\\
% %
% \fun{describe} & From the \pack{Hmisc} package. Gives a statistical summary of a data set. & \makecell{describe(mtcars) \\ describe(airquality)}\\
% %
% \fun{dust} & The basic function for formatting tables using the \pack{pixiedust} package. All other formatting directions ('sprinkles') are passed to this function. & See Section~\ref{sec:markdowntablesword}, p.~\pageref{sec:tableswpdust}\\
% %
% \fun{ftable} & Outputs a 'flat' contingency table, with data cross-tabulated by multiple factors presented in a single table. Particularly useful when used in combination with \fun{xtabs}. & \makecell{table(xtabs( \mytilde{} Class\\ \hskip 2em + Sex + Survived,\\ \hskip 2em data=Titanic))}\\
% %
% \fun{kable} & Can be used to format tables for markdown output. See Section~\ref{sec:markdowntablesword}, p.~\pageref{sec:tableswkable}. & \makecell{kable(with(warpbreaks,\\ \hskip 2em table(wool, tension)))}\\
% %
% \fun{merge} & Merge two dataframes by a common variable. Specifying \code{all=TRUE} keeps all the data, even when some \code{NAs} are created; otherwise any incomplete observations are dropped. & See Section~\ref{sec:merge}, p.~\pageref{page:mergefun}\\
% %
% \fun{pander} & Can be used to format tables for markdown output. Formats a wider range of input than \fun{kable}. For help with \code{pander} tables, use \code{?\fun{pandoc.table}}. & See Section~\ref{sec:markdowntablesword}, p.~\pageref{sec:tableswpander}\\
% %
% \fun{rbind} & Combine two or more dataframes or matrices by binding the rows together. Must have equal column number and names.  & rbind(pupae, pupae) \\
% %
% \fun{rbind.fill} & From the \pack{plyr} package. Combines two or more dataframes or matrices by binding the rows together. Missing observations for variables present in only one dataframe are filled with NAs. & \makecell{rbind.fill(pupae[,-3], \\ \hskip 2em pupae[,-5])} \\
% %
% \fun{reorder} & Reorders levels of a factor based on another variable. & See Section~\ref{sec:reorder}, p.~\pageref{page:reorder}\\
% %
% \fun{str} & Displays the structure of any object. Particularly useful for inspecting variable types in dataframes. & str(pupae) \\
% %
% \fun{summary} & Simple summary table for dataframes  & summary(pupae) \\
% %
% \fun{summaryBy} & From the \pack{doBy} package. Summarizes a dataframe by one or more factors. Output is a dataframe. & \makecell{summaryBy(PupalWeight \mytilde{} \\ \hskip 2em CO2\_treatment,data=pupae, \\ \hskip 2em FUN=c(mean,sd,max))}\\
% %
% \fun{tapply} &  Summarizes a vector by levels of one or more factors, using a function of your choosing. Output is a vector or matrix. For an explanation, see Section~\ref{sec:tapply}, p.~\pageref{sec:tapply}. & \makecell{with(pupae, \\ \hskip 2em tapply(PupalWeight,\\ \hskip 4em Gender, mean))}\\
% %
% \fun{table} & Counts number of observations by levels of one or more factors. & \makecell{with(pupae, \\ \hskip 2em table(CO2\_treatment, \\ \hskip 4em T\_treatment))} \\
% %
% \fun{xtabs} & Cross-tabulation. Like table, but takes a formula (especially useful in conjunction with \code{ftable}). & \makecell{xtabs(\mytilde{} CO2\_treatment + \\ \hskip 2em T\_treatment, \\ \hskip 4em data=pupae)}\\
% %
% \end{functionstable}

\newpage
\section{Exercises}

<<chap6exercises, child="06_summarizedata_Exercises.Rnw", eval=TRUE, echo=FALSE, results='hide', fig.keep='none',warning=FALSE>>=

@

 




