%---------------------------------------------------------------------------%
% Copyright 2015 Remko Duursma, Jeff Powell                                 %
%                                                                           %
% This file is part of HIERmanual                                           %
%                                                                           %
%     HIERmanual is free software: you can redistribute it and/or modify    %
%     it under the terms of the GNU General Public License as published by  %
%     the Free Software Foundation, either version 3 of the License, or     %
%     (at your option) any later version.                                   %
%                                                                           % 
%     HIERmanual is distributed in the hope that it will be useful,         %
%     but WITHOUT ANY WARRANTY; without even the implied warranty of        %
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         %
%     GNU General Public License for more details.                          %
%                                                                           %
%     You should have received a copy of the GNU General Public License     %
%     along with HIERmanual.  If not, see <http://www.gnu.org/licenses/>.   %
%---------------------------------------------------------------------------%

<<child="exercise_colorcodingexplained.Rnw", eval=TRUE>>=
@


\subsection{Alpha diversity}

\subsubsection{Soil fungal data}

\begin{enumerate}

\item \easy Using the data from \file{IxFsub.csv}, produce boxplots showing observed and rarefied richness as a function of \code{Harvest} and \code{Treatment}.

<<>>=

# load data
ixf <- read.csv('IxFsub.csv')

# load 'vegan' library
library(vegan)

# select columns containing species data
ixf.otu <- ixf[, grep('ITSall_OTU', names(ixf))]

# calculate observed and rarefied richness
ixf$rich.obs <- specnumber(ixf.otu)
ixf$rich.chao <- estimateR(ixf.otu)['S.chao1', ]

# produce boxplots
with(ixf, boxplot(rich.obs ~ Treatment + Harvest, las=2, ylab='observed OTU richness'))
with(ixf, boxplot(rich.chao ~ Treatment + Harvest, las=2, ylab='rarefied OTU richness'))


@


\item \easy Fit a linear model and use ANOVA to test for effects and interactions of \code{Harvest} and \code{Treatment} on observed and rarefied richness.

<<>>=

# load 'car' library to get 'Anova' function
library(car)

# fit models
m1.obs <- lm(rich.obs ~ Treatment * Harvest, data=ixf)
m1.chao <- lm(rich.chao ~ Treatment * Harvest, data=ixf)

# produce ANOVA tables
Anova(m1.obs)
Anova(m1.chao)

@


\item \easy The experimental design is actually nested, with samples collected from around two trees in each plot on multiple timepoints. Use \fun{lmer} in the \pack{lme4} package to fit the models in the previous question to also include \code{Plot} and \code{Tree} as random effects. Inspect variation partitioned to random effects and evaluate whether the fixed factors are significant.

<<>>=

# load 'lme4' library 
library(lme4)

# fit models
m1a.obs <- lmer(rich.obs ~ Treatment * Harvest + (1|Plot/Tree), data=ixf)
m1a.chao <- lmer(rich.chao ~ Treatment * Harvest + (1|Plot/Tree), data=ixf)

# inspect random effects block
VarCorr(m1a.obs)
VarCorr(m1a.chao)

# produce ANOVA tables (test.statistic = 'F' calculated KR degrees of freedom)
Anova(m1a.obs, test.statistic = 'F')
Anova(m1a.chao, test.statistic = 'F')

@

\end{enumerate}


\subsection{Ordination}

\subsubsection{Endophyte data}

\begin{enumerate}

\item \easy Read in the data from \file{endophytes.csv}; see Section~\ref{sec:endophyt} (p.~\pageref{sec:endophyt}) for a description of the data. Use the \code{decorana} function to calculate gradient lengths and determine whether PCA is appropriate for these data (see Section~\ref{sec:gradient} for help, if necessary).

<<>>=

# load 'vegan' library
library(vegan)

# read in endophyte community data
endo<-read.csv('endophytes.csv')

# estimate gradient length
decorana(endo) 
# the gradient (axis) lengths are around or greater than 3, 
# suggesting that PCA is not appropriate

@


\item \intermed Perform CA these data and plot the results. Notice the strong skew in the data along both axes. Try again after standardising the community matrix using the \code{decostand} function (try the 'hellinger' and 'max' methods). Notice the undesireable parabolic pattern in the ordination and strong skew; this suggests that CA is not an improvement over PCA (common for data matrices that contain many zeros, collected along long environmental gradients).

<<>>=

# read in endophyte community data
endo<-read.csv('endophytes.csv')

# plot cca results using raw data and following two different standardisation 
# approaches
plot(vegan::cca(endo))
plot(vegan::cca(decostand(endo, method='hellinger')))
plot(vegan::cca(decostand(endo, method='max')))

# how many cells in the matrix are zeros?
summary(as.numeric(endo == 0)) 
# 87% of cells in the matrix equal zero (species is absent)

@


\item \intermed Perform PCoA on these data, using the 'hellinger' method for the \code{decostand} function and 'bray' method for the \code{vegdist()} function, and plot the results. See Section~\ref{sec:pcoa} for help, if necessary. Repeat as before but use the \code{binary} argument in the \code{vegdist} function to convert the matrix to a 'presence/absence' matrix.

<<>>=

# read in endophyte community data
endo<-read.csv('endophytes.csv')

# PCoA with Bray-Curtis dissimilarities
endo.pco.bray<-wcmdscale(vegdist(endo,method='bray'))
# plot PCoA result
plot(endo.pco.bray)

# PCoA with Jaccard index (species presence / absence)
endo.pco.jac<-wcmdscale(vegdist(endo,method='jaccard',binary=T))
# plot PCoA result
plot(endo.pco.jac)

@


\end{enumerate}


\subsection{Analysis of Structure 1: two-table analysis}

\subsubsection{Endophyte data}

\begin{enumerate}

\item \intermed Look at the help page for the \code{capscale} function. Use \code{capscale} to perform distance-based RDA (constrained PCoA) using the continuous variables in \file{endophytes\_env.csv} (percentC, percentN, CNratio) as predictors, then plot the results. First use the \code{envfit} function to determine which variables to include in db-RDA (Section~\ref{sec:constraint}).

<<>>=
# First look at the help page with: ?capscale 

# read in endophyte community data
endo<-read.csv('endophytes.csv')
# read in matrix of predictor variables
endo.env<-read.csv('endophytes_env.csv')

# which environmental variables should be included as predictors?
# first perform PCoA on the community data (input is distance matrix)
endo.pcoa<-wcmdscale(vegdist(endo,method='bray'))
# then use envfit() to see which individual variables are associated with PCoA patterns 
# use scale() to account for differences in variance among variables)
envfit(endo.pcoa, scale(endo.env[,3:5])) 
# all three variable are significant, include all three in analysis

# perform db-RDA (aka CAP) using continuous environmental variables as predictors
endo.cap<-capscale(endo~percentC+percentN+CNratio,data=endo.env,distance='bray')
# look at results
endo.cap  
# 12 % of the variation is explained by C, N, and C:N, 
# most of that variation is accounted for in one axis (CAP1)

# plot results
plot(endo.cap)  
# N and C:N are strongly collinear, 
# C is separated out along the second CAP axis

@

\item \intermed Repeat the analysis in the previous exercise but use the \code{ordistep} function to determine which variables to include in db-RDA.

<<>>=
# First look at the help page with: ?capscale 

# read in endophyte community data
endo<-read.csv('endophytes.csv')
# read in matrix of predictor variables
endo.env<-read.csv('endophytes_env.csv')
# scale continuous variables so variance standardised
endo.env.std <- decostand(endo.env[, 3:5], method='standardize')

# which environmental variables should be included as predictors?
# first perform CAP with each of the environmental variables
endo.cap1<-capscale(endo ~ ., data=endo.env.std, method='bray')
# then perform CAP with no predictors (essentially PCoA, but using the 'capscale' function 
# use scale() to account for differences in variance among variables)
endo.cap0<-capscale(endo ~ 1, data=endo.env.std, method='bray')


# perform forward and backward selection of explanatory variables
step.env <- ordistep(endo.cap0, scope=formula(endo.cap1))

# look at the significant variables (all are significant)
step.env$anova

# view ordination
plot(step.env)

@

\end{enumerate}


\subsection{Analysis of Structure 2: variation partitioning}

\subsubsection{Endophyte data}

\begin{enumerate}

\item \intermed Perform variation partitioning to determine whether leaf species, leaf chemistry, or sample type explains the most variation in fungal community composition.

<<>>=

# load the vegan library
library(vegan)

# read in tables containing species, and environmental variables
endo.spp <- read.csv('endophytes.csv')  # column names represent OTUs
endo.env <- read.csv('endophytes_env.csv')

dim(endo.spp)
str(endo.env)

# select particular variables to proceed with (here we use both forward and backward selection but could use either one separately)

# set up the analysis with all predictors
cap.env <- capscale(endo.spp ~ ., data=endo.env, distance='bray')

# set up the null cases with no predictors
mod0.env <- capscale(endo.spp ~ 1, data=endo.env, distance='bray')

# select variables in each predictor table
step.env <- ordistep(mod0.env, scope=formula(cap.env))

# species, tissue type, tissue CN ratio and N concentration predict variation in community composition
step.env
step.env$anova  # presents results in an ANOVA-like table

# partition variation among four predictor tables:
#   1) leaf species 
#   2) leaf type (canopy/litter)
#   3) leaf chemistry
endo.var <- varpart(endo.spp, 
					~ species, 
					~ type, 
					~ CNratio + percentN, data=endo.env)
endo.var
plot(endo.var)

@

\item \intermed Use the geographic coordinates of each plot to estimate the contribution of space to variation in fungal community composition. Is this estimate greater than the variation partitioned to the measured leaf variables?

<<>>=

# read in table containing geographic distances
endo.dist <- read.csv('endophytes_dist.csv')

str(endo.dist)

# represent spatial patterns through PCNMs
endo.pcnm <- pcnm(dist(endo.dist))
# loadings for each PCNM axis can be extracted using scores()
str(scores(endo.pcnm))

# select particular variables to proceed with (here we use both forward and backward selection but could use either one separately)

# set up the analysis with all predictors
cap.pcnm <- capscale(endo.spp ~ ., data=as.data.frame(scores(endo.pcnm)), distance='bray')

# set up the null cases with no predictors
mod0.pcnm <- capscale(endo.spp ~ 1, data=as.data.frame(scores(endo.pcnm)), distance='bray')

# select variables in each predictor table
step.pcnm <- ordistep(mod0.pcnm, scope=formula(cap.pcnm))

# only six/seven of the PCNM axes appear to predict variation in community composition
# significance of PCNM11 varies each time because it is based on permutations
step.pcnm
step.pcnm$anova  # presents results in an ANOVA-like table

# create pcnm table with only significant axes
endo.pcnm.sub <- scores(endo.pcnm, 
  					choices=c(1:4, 6, 11, 14))

# partition variation among four predictor tables:
#   1) leaf species 
#   2) leaf type (canopy/litter)
#   3) leaf chemistry
#   4) spatial gradients
endo.var <- varpart(endo.spp, 
					~ species, 
					~ type, 
					~ CNratio + percentN, 
					endo.pcnm.sub, data=endo.env)
endo.var
plot(endo.var)

@

\item \intermed Test the significance of each individual partition.

<<>>=

# significance of partition X1
anova(rda(endo.spp	~ species + Condition(endo.env$type) +  
          + Condition(endo.env$CNratio) + Condition(endo.env$percentN) 
          + Condition(endo.pcnm.sub), 
          data=endo.env))

# significance of partition X2
anova(rda(endo.spp	~ type + Condition(endo.env$species) +  
          + Condition(endo.env$CNratio) + Condition(endo.env$percentN) 
          + Condition(endo.pcnm.sub), 
          data=endo.env))

# significance of partition X3
anova(rda(endo.spp	~ CNratio + percentN 
          + Condition(endo.env$species) + Condition(endo.env$type) 
          + Condition(endo.pcnm.sub), 
          data=endo.env))

# significance of partition X4
anova(rda(endo.spp	~ endo.pcnm.sub 
          + Condition(endo.env$species) + Condition(endo.env$type) 
          + Condition(endo.env$CNratio) + Condition(endo.env$percentN)))

@

\item \intermed Generate dummy variables (using 'dudi.hillsmith') for each of the levels of 'species' and check whether there are particular leaf species that explain variation in fungal community composition.

<<>>=

# load library
library(ade4)

# generate new table containing one column for each species
endo.leafspp <- dudi.hillsmith(endo.env[['species']], scannf=F, nf=2)$tab

# set up the analysis with all predictors
cap.spp <- capscale(endo.spp ~ ., data=endo.leafspp, distance='bray')

# set up the null cases with no predictors
mod0.spp <- capscale(endo.spp ~ 1, data=endo.leafspp, distance='bray')

# select variables in each predictor table
step.env <- ordistep(mod0.spp, scope=formula(cap.spp))

# look at ordistep result
step.env$anova  # presents results in an ANOVA-like table
plot(step.env)

@

\end{enumerate}



\subsection{Analysis of Structure 3: 'experimental' systems}

\subsubsection{Allometry data}

\begin{enumerate}

\item \intermed For the allometry data, plot a dendrogram of multivariate distances (euclidean) among individual trees based on the four growth parameters, labelling the tips of the dendrogram with the species level. Use ANOSIM and PERMANOVA to test the hypothesis that clusters can be explained by interspecific variation. See Section~\ref{sec:permanova} for help, if necessary.

<<>>=
library(vegan)

allom<-read.csv('Allometry.csv')

# log-transform the data, then generate distance matrix based on 
# euclidean (geometric) distances
allom.dist <- vegdist(decostand(allom[,2:5],'log'),method='euclidean')
# use hierarchical clustering to determine different levels of similarity among individuals
allom.clust<-hclust(allom.dist)

# If the plotting window is too small, open one like this: windows() 
# (Or click 'Zoom')

# plot the hierarchical clustering result, specifying species with 
# the 'labels' argument.
plot(allom.clust,labels=allom[,1])

# estimate the significance of tree species as a predictor of multivariate growth response
# using ANOSIM
allom.ano<-anosim(allom.dist,allom[,1])
summary(allom.ano)  # p-value is nonsignificant
# using PERMANOVA
adonis(allom.dist~allom[,1])  # p-value is nonsignificant
# variation between species is similar to variation within species
@

\item \intermed Using your knowledge from Chapter~\ref{chap:plotting} and Sections~\ref{sec:nmds} and \ref{sec:permanova}, plot the ordination results using coloured circles to represent the different tree species and include a legend. 

<<>>=
library(vegan)

allom<-read.csv('Allometry.csv')

#log-transform the data prior to PCA
allom.pca.log<-prcomp(log(allom[,2:5]),scale=T)

#use the scores argument to extract the site loadings; 
# we want the first two columns (PC1, PC2)
allom.scores<-scores(allom.pca.log)[,1:2]

#use 'plot' to plot the data and index the 'col' argument by 'species'
palette(c("blue","red","forestgreen"))  # set the colour palette to these three colours
plot(allom.scores, pch=16, col=allom$species)  # 'pch=16' results in closed circles

#add a legend
legend('topright', legend=levels(allom$species), pch=16, col=palette())
@

\item \hard Overlay the plot with the centroid (i.e., average) for each species, using a different symbol than for the individual points. Modify the axes to reflect the percentage of inertia (i.e., variance) explained by each axis. Refer to Chapter~\ref{chap:summarize} for help tabulating mean values, if necessary.

<<>>=
library(vegan)

allom<-read.csv('Allometry.csv')

# log-transform the data prior to PCA
allom.pca.log<-prcomp(log(allom[,2:5]),scale=T)

# use the scores argument to extract the site loadings; 
# we want the first two columns (PC1, PC2)
allom.scores<-scores(allom.pca.log)[,1:2]

#estimate mean associated with each species, using aggregate on the PCA result
allom.agg<-aggregate(allom.scores, by=list(species=allom$species), FUN=mean)

# proportion of inertia explained by each axis can be found using the 'summary' argument
summary(allom.pca.log)
# using 'str()', we see that the summary object is a named list, 
# we need to extract the 'importance' element
str(summary(allom.pca.log))
# the relevant proportions for each axis can be found in the second row; 
# we want the first two columns
allom.pcvar<-summary(allom.pca.log)$importance[2,1:2]

# use 'plot' to plot the data and index the 'col' argument by 'species', 
# customise the axis labels using 'paste'
palette(c("blue","red","forestgreen"))
plot(allom.scores, pch=16, col=allom$species, 
	 xlab=paste('PC1 (', 100*round(allom.pcvar[1], 3), ' %)', sep=''),
	 ylab=paste('PC2 (', 100*round(allom.pcvar[2], 3), ' %)', sep='')
	 )

# add a legend
legend('topright', legend=levels(allom$species), pch=16, col=palette())

# use 'points' to overlay the centroids over the plot
points(allom.agg[,2:3], pch=17, cex=2, col=palette())

@


\end{enumerate}

\subsubsection{Endophyte data}

\begin{enumerate}

\item \intermed Use the \code{adonis} function to test for main and interactive effects of tree species and tissue type (fresh vs litter) on fungal community composition (see Section~\ref{sec:permanova}). The predictor variables can be found in \file{endophytes\_env.csv}. What terms were significant? Which term explained the most variation?

<<>>=

# read in endophyte community data
endo<-read.csv('endophytes.csv')
# read in matrix of predictor variables
endo.env<-read.csv('endophytes_env.csv')

# use PERMANOVA to test statistical significance of tree species and 
# tissue type, interaction.
# Use the formula interface, response is a distance matrix
adonis(vegdist(endo,method='bray') ~ type * species, data=endo.env) 
# all terms were significant, species had the largest R2

@

\item \hard Plot the PCoA results and use different symbols and colours to reflect the identities of tree species and tissue types. Add a legend to the plot. Use information from Chapter~\ref{chap:plotting} and Sections~\ref{sec:nmds} and \ref{sec:permanova} for help, if necessary. Hint: automatic functions for generating vectors of colours, such as \fun{rainbow}, can lead to very similar colours with so many treatment levels. Check out the \pack{randomcoloR} package for alternative approaches.

<<>>=
# read in endophyte community data
endo<-read.csv('endophytes.csv')
# read in matrix of predictor variables
endo.env<-read.csv('endophytes_env.csv')

# perform PCoA, input is a distance matrix
endo.pcoa<-wcmdscale(vegdist(endo,method='bray'))

# set up palette
library(randomcoloR)
palette(randomColor(length(levels(endo.env$species))))

# plot PCoA results using scores() to extract the site loadings
# eight colours indexed by species, two symbols indexed by tissue type
plot(scores(endo.pcoa,display='sites'),col=endo.env$species,
     pch=c(1,16)[endo.env$type])
# add a legend indicating tissue type
legend('bottomleft',levels(endo.env$type),pch=c(1,16))
# add a legend indicating tree species
legend('bottomright',levels(endo.env$species),col=palette(),pch=16,cex=0.75)

@

\item \hard Overlay the plot with ellipses representing 95\% confidence intervals for each species and sample type using functions seen in section \ref{sec:permanova}.

<<>>=
# create vector identifying unique treatment combinations
trts <- with(endo.env, interaction(type, species, sep='-'))
levels(trts)

# set up palette
library(randomcoloR)
palette(randomColor(length(levels(endo.env$species))))

# set up vectors for colours and line types based on treatment combination levels
col.spp <- rep(palette(), each=2)
lty.type <- rep(c('dashed', 'solid'), 9)

# plot PCoA results using scores() to extract the site loadings
# eight colours indexed by species, two symbols indexed by tissue type
plot(scores(endo.pcoa,display='sites'),col=endo.env$species,
     pch=c(1,16)[endo.env$type])
# add a legend indicating tissue type
legend('bottomleft',levels(endo.env$type),pch=c(1,16))
# add a legend indicating tree species
legend('bottomright',levels(endo.env$species),col=palette(),pch=16,cex=0.75)

# plot ellipses, indexing colours by species and line types by sample type
ordiellipse(endo.pcoa, trts, kind='se', conf=0.95, 
            col=col.spp, lty=lty.type)
@

\end{enumerate}

\subsubsection{Mite data}

\begin{enumerate}

\item \easy Use \code{manova} to estimate the responses of mite community composition to the environmental variables associated with the \code{mite} data. 

<<>>=

# load library
library(vegan)

# load 'mite' data
data(mite)
data(mite.env)

# convert response table to matrix
Y <- as.matrix(mite)

# fit model
mite.manova <- manova(Y ~ SubsDens + WatrCont + Substrate + Shrub + Topo, 
                     data = mite.env)


# view model summary
summary(mite.manova)

@

\item \intermed There is not a built in function to view diagnostic plots for \code{manova} output. Use the \code{resid} function to obtain the residuals for each response variable and use the \code{qqnorm} and \code{qqline} functions to produce quantile-quantile plots for a few of the response variables to determine whether it is appropriate to model the responses using a normal error distribution.

<<>>=

# plot residuals for first response variable
qqnorm(resid(mite.manova)[, 1])
# add line
qqline(resid(mite.manova)[, 1])

@

\item \intermed Use \code{manyglm} to estimate the responses of mite community composition to the environmental variables associated with the \code{mite} data. Which error family, poisson or negative binomial, provides the best fit to the data? Look at the results of the best fitting model.

<<>>=

# load libraries
library(vegan)
library(mvabund)

# load 'mite' data
data(mite)
data(mite.env)

# convert response table to an mvabund object
mitedat <- mvabund(mite)

# fit multivariate GLM model with poisson error distribution
mite.pois <- manyglm(mitedat ~ SubsDens + WatrCont + Substrate + Shrub + Topo, 
                     data = mite.env, family='poisson')

# fit multivariate GLM model with negative binomial error distribution
mite.nbin <- manyglm(mitedat ~ SubsDens + WatrCont + Substrate + Shrub + Topo, 
                     data = mite.env, family='negative.binomial')

# view model diagnostics
plot(mite.pois)
plot(mite.nbin)

# view model summary and ANOVA table for 'negative binomial' model
summary(mite.nbin)
anova(mite.nbin)

@


\end{enumerate}