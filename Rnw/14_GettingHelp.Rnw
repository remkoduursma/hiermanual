
%---------------------------------------------------------------------------%
% Copyright 2015 Remko Duursma, Jeff Powell                                 %
%                                                                           %
% This file is part of HIERmanual                                           %
%                                                                           %
%     HIERmanual is free software: you can redistribute it and/or modify    %
%     it under the terms of the GNU General Public License as published by  %
%     the Free Software Foundation, either version 3 of the License, or     %
%     (at your option) any later version.                                   %
%                                                                           % 
%     HIERmanual is distributed in the hope that it will be useful,         %
%     but WITHOUT ANY WARRANTY; without even the implied warranty of        %
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         %
%     GNU General Public License for more details.                          %
%                                                                           %
%     You should have received a copy of the GNU General Public License     %
%     along with HIERmanual.  If not, see <http://www.gnu.org/licenses/>.   %
%---------------------------------------------------------------------------%

\chapter{Getting Help}

\section{Web}

That's right, Google should be your number one choice. Try typing a question in full, for example, "How to read tab-delimited data in R". \\
\url{www.google.com}

Search engine for \R{}-related topics (including packages, blogs, email lists, etc.):\\
\url{www.rseek.org}

Excellent resource for graphical parameters, like par() (and more resources on \R{}):\\
\url{http://www.statmethods.net/advgraphs/parameters.html}

Archive (and search) of the R-help mailing list can be found on Nabble:\\
\url{http://r.789695.n4.nabble.com/}

Search engine for the R-help mailing list:\\
\url{http://finzi.psych.upenn.edu/search.html}


\section{Free online books}

You can find a large number of manuals on the CRAN website. There, find the link 'Contributed' on the left:\\
\url{www.cran.r-project.org}

If you are interested in the inner workings of \R{}, the 'R inferno' is not to be missed:\\
\url{http://www.burns-stat.com/pages/Tutor/R_inferno.pdf}


\section{Blogs}

A compendium of blogs about \R{} can be found here. These blogs include many posts on frequently asked questions, as well as advanced topics: \\
\url{http://www.r-bloggers.com/}

Examples of various simple or complicated plots that have been produced using \R{}, with code provided can be found here:\\
\url{http://rgraphgallery.blogspot.com.au/}


\section{Finding packages}

<<echo=FALSE, message=FALSE, eval=FALSE>>=
get_nrcranpack <- function(cachefile="../cache/cranpack"){
  
  if(file.exists(cachefile))
      nrpack <- as.numeric(readLines(cachefile))
  else 
      nrpack <- 8364
  
  r <- require(xml2)
  if(!r)return(nrpack)
  
  read_web_cran <- function(){
    web <- xml2::read_html("https://cran.r-project.org/web/packages/")
    packstr <- stringr::str_extract(xml2::xml_text(web),
                                    "repository features [0-9]{4,5} available packages")
    as.numeric(stringr::str_extract(packstr, "[0-9]{4,5}"))
  }
  nrpacknow <- try(read_web_cran())
  if(!inherits(nrpacknow,"try-error")){
    writeLines(as.character(nrpacknow),cachefile)
    return(nrpacknow)
  } else {
    return(nrpack)
  }
}
@


With >10000 packages available on CRAN alone (\url{http://cran.r-project.org/}), it can be difficult to find a package to perform a specific analysis. Often the easiest way to find a package is to use Google (for example, 'r package circular statistics'). CRAN has also categorized many packages in their 'Task Views':\\
\url{http://cran.r-project.org/web/views/}\\
For example, the 'Spatial' page lists over 130 packages to do with various aspects of spatial analysis and visualization.


\section{Offline paper books}

An exhaustive list of printed books can be found here: \\
\url{http://www.r-project.org/doc/bib/R-books.html}

Springer publishes many books about \R{}, incluing a special series called "Use R!" If your library has a subscription, many of these books can be read online. For example, the classic \emph{Introductory Statistics with R} by Peter Dalgaard: \\
\url{http://link.springer.com/book/10.1007/978-0-387-79054-1/page/1}


