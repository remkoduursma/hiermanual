%---------------------------------------------------------------------------%
% Copyright 2015 Remko Duursma, Jeff Powell                                 %
%                                                                           %
% This file is part of HIERmanual                                           %
%                                                                           %
%     HIERmanual is free software: you can redistribute it and/or modify    %
%     it under the terms of the GNU General Public License as published by  %
%     the Free Software Foundation, either version 3 of the License, or     %
%     (at your option) any later version.                                   %
%                                                                           % 
%     HIERmanual is distributed in the hope that it will be useful,         %
%     but WITHOUT ANY WARRANTY; without even the implied warranty of        %
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         %
%     GNU General Public License for more details.                          %
%                                                                           %
%     You should have received a copy of the GNU General Public License     %
%     along with HIERmanual.  If not, see <http://www.gnu.org/licenses/>.   %
%---------------------------------------------------------------------------%

\chapter{Description of datasets}
\label{sec:datasets}

%----------------------------------------------------------------------------------
\section{Tree allometry}
\label{sec:allomdata}

\textbf{File:} \file{Allometry.csv}

This dataset contains measurements of tree dimensions and biomass. Data kindly provided by John Marshall, University of Idaho.

\textbf{Variables:}
\begin{itemize}
\item \code{species} - The tree species (PSME = Douglas fir, PIMO = Western white pine, PIPO = Ponderosa pine).
\item \code{diameter} - Tree diameter at 1.3m above ground ($cm$).
\item \code{height} - Tree height ($m$).
\item \code{leafarea} - Total leaf area ($m^2$).
\item \code{branchmass} - Total (oven-dry) mass of branches ($kg$).
\end{itemize}

%----------------------------------------------------------------------------------
\section{Coweeta tree data}
\label{sec:coweetadata}

\textbf{File:} \file{coweeta.csv}

Tree measurements in the Coweeta LTER (\url{http://coweeta.uga.edu/}). Data are from Martin et al. (1998; Can J For Res 28:1648-1659).

\textbf{Variables:}
\begin{itemize}
\item \code{species} One of 10 tree species
\item \code{site} Site abbreviation
\item \code{elev} Elevation ($m$ asl)
\item \code{age} Tree age ($yr$)
\item \code{DBH} Diameter at breast height ($cm$)
\item \code{height} Tree height ($m$)
\item \code{folmass} Foliage mass ($kg$)
\item \code{SLA} Specific leaf area ($cm g^-1$)
\item \code{biomass} Total tree mass ($kg$)
\end{itemize}

%----------------------------------------------------------------------------------
\section{Hydro dam}
\label{sec:hydrodata}

\textbf{File:} \file{hydro.csv}


This dataset describes the storage of the hydrodam on the Derwent river in Tasmania (Lake King William \& Lake St. Clair), in equivalent of energy stored. Data were downloaded from \url{http://www.hydro.com.au/water/energy-data}.


\textbf{Variables:}
\begin{itemize}
\item \code{Date} - The date of the bi-weekly reading
\item \code{storage} - Total water stored, in energy equivalent ($GWh$).
\end{itemize}


%----------------------------------------------------------------------------------
\section{Rain}
\label{sec:raindata}
\textbf{File:} \file{Rain.csv}


This dataset contains ten years (1995-2006) of daily rainfall amounts as measured at the Richmond RAAF base.
Source : BOM (\url{http://www.bom.gov.au/climate/data/}).

\textbf{Variables:}
\begin{itemize}
\item \code{Year}
\item \code{DOY} - Day of year (1-366)
\item \code{Rain} - Daily rainfall ($mm$)
\end{itemize}

%----------------------------------------------------------------------------------
\section{Weight loss}
\label{sec:weightlossdata}

\textbf{File:} \file{Weightloss.csv}

This dataset contains measurements of a Jeremy Zawodny over a period of about 3 months while he was trying to lose weight (data obtained from \url{http://jeremy.zawodny.com/blog/archives/006851.html}). This is an example of an irregular timeseries dataset (intervals between measurements vary).

\textbf{Variables:}
\begin{itemize}
\item \code{Date} - The Date (format, dd/mm/yy)
\item \code{Weight} - The person's weight ($lbs$)
\end{itemize}

%----------------------------------------------------------------------------------
\section{Pupae}
\label{sec:pupaedata}
\textbf{File:} \file{pupae.csv}

This dataset is from an experiment where larvae were left to feed on \emph{Eucalyptus} leaves,
in a glasshouse that was controlled at two different levels of temperature and CO$_2$ concentration.
After the larvae pupated (that is, turned into pupae), the body weight was measured, as well as the cumulative 'frass' (larvae excrement) over the entire time it took to pupate.

Data courtesy of Tara Murray, and simplified for the purpose of this book.

\textbf{Variables:}
\begin{itemize}
\item \code{T\_treatment} - Temperature treatments ('ambient' and 'elevated')
\item \code{CO2\_treatment} - CO$_2$ treatment (280 or 400 ppm).
\item \code{Gender} - The gender of the pupae : 0 (male), 1 (female)
\item \code{PupalWeight} - Weight of the pupae ($g$)
\item \code{Frass} - Frass produced ($g$)
\end{itemize}

%----------------------------------------------------------------------------------
\section{Cereals}
\label{sec:cerealdata}

\textbf{File:} \file{Cereals.csv}

This dataset summarizes 77 different brands of breakfast cereals, including calories,
proteins, fats, and so on, and gives a 'rating' that indicates the overall nutritional value of the cereal.

This dataset was downloaded from Statlib at CMU, and is frequently used as an example dataset.


\textbf{Variables:}
\begin{itemize}
\item \code{Cereal name} - Name of the cereal (text)
\item \code{Manufacturer} - One of: "A","G","K","N","P","Q","R" (A = American Home Food Products; G = General Mills; K = Kelloggs; N = Nabisco; P = Post; Q = Quaker Oats; R = Ralston Purina)
\item \code{Cold or Hot} - Either "C" (cold) or "H" (Hot).
\item \code{calories}  - number
\item \code{protein} - g
\item \code{fat} - g
\item \code{sodium} - g
\item \code{fiber} - g
\item \code{carbo} - g
\item \code{sugars} - g
\item \code{potass} - g
\item \code{vitamins} - 0,25 or 100
\item \code{rating} - Nutritional rating (function of the above 8 variables).
\end{itemize}

\emph{NOTE:} also included are the files \file{cereal1.csv}, \file{cereal2.csv} and \file{cereal3.csv}, small subsets of the cereal data used in Section~\ref{sec:multmerge}.


%----------------------------------------------------------------------------------
\section{Flux tower data}
\label{sec:fluxdata}

\textbf{File:} \file{Fluxtower.csv}

This dataset contains measurements of CO$_2$ and H$_2$O fluxes (and related variables) over a pine forest in Quintos de Mora, Spain. The site is a mixture of \emph{Pinus pinaster} and \emph{Pinus pinea}, and was planted in the 1960's.

Data courtesy of Victor Resco de Dios, and simplified for the purpose of this book.

\textbf{Variables:}
\begin{itemize}
\item \code{TIMESTAMP} - Character vector with date and time
\item \code{FCO2} - Canopy CO2 flux ($\mu$ mol m$^{-2}$ s$^{-1}$)
\item \code{FH2O} - Canopy H2O flux (mmol m$^{-2}$ s$^{-1}$)
\item \code{ustar} - Roughness length (m s$^{-1}$)
\item \code{Tair} - Air temperature (degrees C)
\item \code{RH} - Relative humidity (\%)
\item \code{Tsoil} - Soil temperature (degrees C)
\item \code{Rain} - Rainfall (mm half hour$^{-1}$)
\end{itemize}


%----------------------------------------------------------------------------------

\section{Pulse rates before and after exercise}
\label{sec:pulsedata}

\textbf{File:} \file{ms212.txt}  


This data is in a TAB separated file; to read it in use \code{read.table("ms212.txt", header=TRUE)}.

A dataset on pulse rates before and after exercise. Taken from the OzDASL library
(Smyth, GK (2011). Australasian Data and Story Library (OzDASL). \url{http://www.statsci.org/data}.)


More information at \url{http://www.statsci.org/data/oz/ms212.html}.

\textbf{Variables:}
\begin{itemize}
\item \code{Height} - Height (cm)
\item \code{Weight}	- Weight (kg)
\item \code{Age} - Age (years)
\item \code{Gender} -	Sex (1 = male, 2 = female)
\item \code{Smokes}	- Regular smoker? (1 = yes, 2 = no)
\item \code{Alcohol} - Regular drinker? (1 = yes, 2 = no)
\item \code{Exercise} - Frequency of exercise (1 = high, 2 = moderate, 3 = low)
\item \code{Ran} - Whether the student ran or sat between the first and second pulse measurements (1 = ran, 2 = sat)
\item \code{Pulse1} - First pulse measurement (rate per minute)
\item \code{Pulse2} - Second pulse measurement (rate per minute)
\item \code{Year} - Year of class (93 - 98)
\end{itemize}

%----------------------------------------------------------------------------------------

\section{Weather data at the HFE}
\label{sec:hfemet}

\textbf{File:} \file{HFEmet2008.csv}

Data for the weather station at the Hawkesbury Forest Experiment for the year 2008.

Data courtesy of Craig Barton. 

\textbf{Variables:}
\begin{itemize}
\item \code{DateTime} - Date Time (half-hourly steps)
\item \code{Tair} - Air temperature (degrees C)
\item \code{AirPress} - Air pressure (kPa)
\item \code{RH} - Relative humidity (\%)
\item \code{VPD} - Vapour pressure deficit (kPa)
\item \code{PAR} - Photosynthetically active radiation ($\mu$ mol m$^{-2}$ s$^{-1}$)
\item \code{Rain} - Precipitation (mm)
\item \code{wind} - Wind speed (m s$^{-1}$)
\item \code{winddirection} - Wind direction (degrees)
\end{itemize}

%----------------------------------------------------------------------------------------

\section{Age and memory}
\label{sec:agemem}

\textbf{File:} \file{eysenck.txt}
This data is in a TAB separated file, so to read it in use \code{read.table("eysenck.txt", header=TRUE)}.

A dataset on the number of words remembered from list, for various learning techniques, and in two age groups.
Taken from the OzDASL library
(Smyth, GK (2011). Australasian Data and Story Library (OzDASL). \url{http://www.statsci.org/data}.)

More information at \url{http://www.statsci.org/data/general/eysenck.html}.

\textbf{Variables:}
\begin{itemize}
\item \code{Age} - Younger or Older
\item \code{Process} - The level of processing: Counting, Rhyming, Adjective, Imagery or Intentional
\item \code{Words} - Number of words recalled
\end{itemize}


%----------------------------------------------------------------------------------------

\section{Passengers on the Titanic}
\label{sec:titanic}

\textbf{File:} \file{titanic.txt}  


This data is in a TAB separated file, so to read it in use \code{read.table("titanic.txt", header=TRUE)}.

Survival status of passengers on the Titanic, together with their names, age, sex and passenger class.
Taken from the OzDASL library
(Smyth, GK (2011). Australasian Data and Story Library (OzDASL). \url{http://www.statsci.org/data}.)

More information at \url{http://www.statsci.org/data/general/titanic.html}.

\textbf{Variables:}
\begin{itemize}
\item \code{Name}  	Recorded name of passenger
\item \code{PClass}		Passenger class: 1st, 2nd or 3rd
\item \code{Age}		Age in years (many missing)
\item \code{Sex}		male or female
\item \code{Survived}		1 = Yes, 0 = No
\end{itemize}



%----------------------------------------------------------------------------------
\section{Xylem vessel diameters}
\label{sec:vesseldata}

\textbf{File:} \file{vessel.csv}

Measurements of diameters of xylem (wood) vessels on a single \emph{Eucalyptus saligna} tree grown at the Hawkesbury Forest Experiment. 

Data courtesy of Sebastian Pfautsch.

\textbf{Variables:}
\begin{itemize}
\item \code{position} - Either 'base' or 'apex' : the tree was sampled at stem base and near the top of the tree.
\item \code{imagenr} - At the stem base, six images were analyzed (and all vessels measured in that image). At apex, three images.
\item \code{vesselnr} - Sequential number 
\item \code{vesseldiam} - Diameter of individual vessels ($\mu$ m).
\end{itemize}



%----------------------------------------------------------------------------------------

\section{Eucalyptus leaf endophytes}
\label{sec:endophyt}

\textbf{File:} \file{endophytes\_env.csv} and \file{endophytes.csv}

Community fingerprints from fungi colonising living leaves and litter from nine Eucalyptus spp. in the HFE common garden and surrounding area. \file{endophytes.csv} contains a 'species-sample' matrix, with 98 samples in rows and 874 operational taxonomic units (OTUs) in columns.

The variables below refer to the data in \file{endophytes\_env.csv}. The rows are matched across the two tables. 

\textbf{Variables:}
\begin{itemize}
\item \code{species} - Tree species
\item \code{type} - Whether leaves came from canopy (fresh) or ground (litter)
\item \code{percentC} - Leaf carbon content, per gram dry mass
\item \code{percentN} - Leaf nitrogen content, per gram dry mass
\item \code{CNratio} - Ratio of C to N in leaves
\end{itemize}

%----------------------------------------------------------------------------------
\section{I x F at the HFE - plot averages}
\label{sec:ifdata}

\textbf{File:} \file{HFEIFplotmeans.csv}

Tree inventory data from the irrigation by fertilization (I x F) experiment in the Hawkesbury Forest Experiment (HFE). This dataset includes the plot means, see I x F tree data for the tree-level observations.

Data courtesy of Craig Barton and Burhan Amiji.

\textbf{Variables:}
\begin{itemize}
\item \code{plotnr} - A total of sixteen plots (four treatments).
\item \code{Date} - The date of measurement.
\item \code{totalvolume} - Total estimated woody volume (m$^3$ ha$^{-1}$).
\item \code{meandiameter} - Mean diameter for the sample trees ($cm$).
\item \code{meanheight} - Mean height for the sample trees ($m$).
\item \code{treat} - One of four treatments (I - irrigated, F - dry fertilized, IL - Liquid fertilizer plus irrigation, C - control).
\end{itemize}

%----------------------------------------------------------------------------------
\section{I x F at the HFE - tree observations}
\label{sec:ifdatatree}

\textbf{File:} \file{HFEIFbytree.csv}

Tree inventory data from the irrigation by fertilization (I x F) experiment in the Hawkesbury Forest Experiment (HFE). This dataset includes the tree-level observations.

Data courtesy of Craig Barton and Burhan Amiji.

\textbf{Variables:}
\begin{itemize}
\item \code{ID} A unique identifier for each tree.
\item \code{plotnr} - A total of sixteen plots (four treatments).
\item \code{treat} - One of four treatments (I - irrigated, F - dry fertilized, 
IL - Liquid fertilizer plus irrigation, C - control)
\item \code{Date} - The date of measurement (YYYY-MM-DD)
\item \code{height} - Mean height for the sample trees ($m$).
\item \code{diameter} - Mean diameter for the sample trees ($cm$).
\end{itemize}


%----------------------------------------------------------------------------------

\section{Dutch election polls}
\label{sec:electiondata}

\textbf{File:} \file{dutchelection.csv}

Polls for the 12 leading political parties in the Netherlands, leading up to the general election on 12 Sept. 2012. Data are in 'wide' format, with a column for each party. Values are in percentages.

Data taken from \url{http://en.wikipedia.org/wiki/Dutch_general_election,_2012}, based on polls by Ipsos NL.


%----------------------------------------------------------------------------------------

\section{Tibetan Plateau plant community data}
\label{sec:tibplat}

\textbf{File:} \file{tibplat.csv}

Plant community data collected from 0.5 m by 0.5 m plots in an alpine grassland in the Tibetan Plateau. The experiment included two factors: fertilisation (none, 30 grams nitrogen per square metre) and grazing (enclosed to prevent grazing, not enclosed). This dataset is a subset of the whole dataset, including only the eight most abundant species of the 48 species measured withing the plots. The full dataset was analysed in Yang et al., 2012, Ecology 93:2321 (doi:10.1890/11-2212.1).

\textbf{Variables:}
\begin{itemize}
\item \code{fertilization} - 1:yes, 0:no
\item \code{enclosure} - 1:yes, 0:no
\item columns 3:50 - aboveground biomass per plot; column heading indicates species identity
\end{itemize}


%----------------------------------------------------------------------------------------

\section[Genetically modified soybean litter decomposition]{Genetically modified soybean\\ litter decomposition}
\label{sec:masslost}
 
\textbf{File:} \file{masslost.csv}

Soybean litter decomposition as a function of time (\code{date}), type of litter (\code{variety}), herbicides applied (\code{herbicide}), and where in the soil profile it is placed (\code{profile}). \code{masslost} refers to the proportion of the litter that was lost from the bag (decomposed) relative to the start of the experiment. Herbicide treatments were applied at the level of whole plots, with both treatments represented within each of four blocks. Both levels of variety and profile were each represented within each plot, with six replicates of each treatment added to each plot. 

\textbf{Variables:}
\begin{itemize}
\item \code{plot} - A total of eight plots.
\item \code{block} - A total of four blocks.
\item \code{variety} - Soybean variety is genetically modified ('gm') or not ('nongm'); manipulated at the subplot level.
\item \code{herbicide} - Herbicide applied is glyphosate ('gly') or conventional program ('conv'); manipulated at plot level.
\item \code{profile} - Whether litter was 'buried' in the soil or placed at the soil 'surface'; manipulated at the subplot level.
\item \code{date} - Date at which litter bags were recovered.
\item \code{sample} - Factor representing timing of sampling ('incrop1', 'incrop2', 'postharvest').
\item \code{masslost} - The proportion of the initial mass that was lost from each litter bag during field incubation. Some values are lower than zero due to insufficient washing of dirt and biota from litter prior to weighing.
\end{itemize}


%----------------------------------------------------------------------------------
\section{EucFACE ground cover data}
\label{sec:groundcover}

\textbf{File:} \file{eucfaceGC.csv}

This file contains estimates of plant and litter cover within the rings of the EucFACE experiment, evaluating forest ecosystem responses to elevated CO$_2$, on two dates. Within each ring are four plots and within each plot are four 1m by 1m subplots. Values represent counts along a grid of 16 points within each subplot.

\textbf{Variables:}
\begin{itemize}
\item \code{Date} - Date at which measurements took place.
\item \code{Ring} - The identity of the EucFACE Ring, the level at which the experimental treatment is applied.
\item \code{Plot} - A total of four plots, nested within each level of Ring.
\item \code{Sub} - A total of four subplots, nested within each level of Plot.
\item \code{Forbes} - Number of points where dicot plants are observed.
\item \code{Grass} - Number of points where grass is observed.
\item \code{Litter} - Number of points where leaf litter is observed.
\item \code{Trt} - The experimental treatment: \code{ctrl} for ambient levels of atmospheric carbon dioxide, \code{elev} for ambient plus 150ppm.
\end{itemize}

%----------------------------------------------------------------------------------
\section{Tree canopy gradients in the Priest River Experimental Forest (PREF)}
\label{sec:prefdata}

\textbf{File:} \file{prefdata.csv}

The dataset contains measurements of leaf mass per area (LMA), and distance from the top of the tree (dfromtop) on 35 trees of two species. 

Data courtesy of John Marshall (Marshall, J.D., Monserud, R.A. 2003. Foliage height influences specific leaf area of three conifer species. Can J For Res 33:164-170), and simplified for the purpose of this book.

\textbf{Variables:}
\begin{itemize}
\item \code{ID} - ID of the individual tree
\item \code{species} - Pinus ponderosa or Pinus monticola
\item \code{dfromtop} - Distance from top of tree (where leaf sample was taken) (m)
\item \code{totheight} - Total height of the tree (m)
\item \code{height} - Height from the ground (where sample was taken) (m)
\item \code{LMA} - Leaf mass per area (g m$^{-2}$)
\item \code{narea} - Nitrogen per area (gN m$^{-2}$)
\end{itemize}


%----------------------------------------------------------------------------------
\section{Seed germination}
\label{sec:seedgerminationdata}
\textbf{File:} \file{germination\_fire.csv} and \file{germination\_water.csv}

Two datasets on the germination success of seeds of four \emph{Melaleuca} species, when subjected to temperature, fire cue, and dehydration treatments. Seeds were collected from a number of sites and subjected to 6 temperature treatments and fire cues (in the fire germination data), or two a range of dehydration levels (in the water germination data).

Data are from Hewitt et al. 2015 (Austral Ecology 40(6):661-671), shared by Charles Morris, and simplified for the purpose of this book.

\textbf{Variables:}

\file{germination\_fire.csv} :
\begin{itemize}
\item \code{species} - One of four Melaleuca species
\item \code{temp} - Temperature treatment (C)
\item \code{fire.cues} - Fire cue treatment (yes or no)
\item \code{site} - Coding for the site where the seed was collected
\item \code{cabinet} - ID for the cabinet where seeds were treated
\item \code{germ} - Number of germinated seeds
\item \code{n} - Number of seeds tested (20 for all rows)
\end{itemize}

\file{germination\_water.csv} :
\begin{itemize}
\item \code{species} - One of four Melaleuca species
\item \code{site} - Coding for the site where the seed was collected
\item \code{water.potential} - Water potential of the seed (Mpa) after incubation (low values is drier)
\item \code{germ} - Number of germinated seeds
\item \code{n} - Number of seeds tested (25 for all rows)
\end{itemize}


%----------------------------------------------------------------------------------
\section{Leaf gas exchange at the EucFACE}
\label{sec:eucgasdata}
\textbf{File:} \file{eucface\_gasexchange.csv}

Measurements of leaf net photosynthesis at the EucFACE experiment, on leaves of different trees growing in ambient and elevated CO$_2$ concentrations. Measurements were repeated four times during 2013 (labelled as Date=A,B,C,D). 

Data are from Gimeno et al. 2015 (Functional Ecology,  doi: 10.1111/1365-2435.12532), and simplified for the purpose of this book.

\textbf{Variables:}
\begin{itemize}
\item \code{Date} - One of four campaigns (A,B,C,D)
\item \code{CO2} - CO2 treatment (either ambient - Amb, or elevated - Ele)
\item \code{Ring}- One of six 'rings' (plots) at the EucFACE 
\item \code{Tree} - Unique identifier for the tree number
\item \code{Photo} - Net leaf photosynthesis ($\mu$ mol m$^{-2}$ s$^{-1}$)
\item \code{Trmmol} - Leaf transpiration rate (mmol m$^{-2}$ s$^{-1}$)
\item \code{VpdL} - Vapour pressure deficit (kPa)
\end{itemize}

%----------------------------------------------------------------------------------
\section{Howell height, age and weight data}
\label{sec:howell}

\textbf{File:} \file{howell.csv}

Downloaded from \url{https://tspace.library.utoronto.ca/handle/1807/17996}, subsetted for non-missing data and one outlier removed. These data were also used by McElreath (2016, "Statistical Rethinking", CRC Press). Data include measurements of height, age and weight on Khosan people. 

\textbf{Variables:}
\begin{itemize}
\item \code{sex} - male or female
\item \code{age} - Age (years) 
\item \code{weight} - Body weight )kg)
\item \code{height} - Total height (cm)
\end{itemize}


%----------------------------------------------------------------------------------
\section{Wild mouse metabolism}
\label{sec:wildmouse}

\textbf{File:} \file{wildmousemetabolism.csv}

Data courtesy of Chris Turbill.

rmr resting metabolic rate minimum of a running average over 12min (kC hour-1)
each run is six days
bm body mass in grams
food


\textbf{Variables:}
\begin{itemize}
\item \code{id} - Individual number.
\item \code{run} - The experiment was repeated three times (run = 1,2,3)
\item \code{day} - Day of experiment (1-6)
\item \code{temp} - Temperature (deg C)
\item \code{food} - Whether food was provided ('Yes') or not ('No')
\item \code{bm} - Body mass (g)
\item \code{wheel} - Whether the mouse could use an exercise wheel ('Yes') or not ('No')
\item \code{rmr} -  Resting metabolic rate (minimum rate of a running average over 12min) (kC hour-1)
\item \code{sex} - Male or Female 
\end{itemize}

%----------------------------------------------------------------------------------
\section{Plant drought tolerance}
\label{sec:choatp50data}

\textbf{File:} \file{Choat\_precipP50.csv}

Data are from Choat et al. 2012 (Nature 491: 752–755), and were simplified for the purpose of this book.

Data include a measure of plant drought tolerance (P50, more negative values indicate plant stems can tolerate lower water contents), and mean annual precipitation of the location where the sample was taken. Data are for 115 individual species (species name not included).

\textbf{Variables:}
\begin{itemize}
\item \code{annualprecip} - Mean annual precipitation (mm)
\item \code{P50} - Measure of plant drought tolerance (the water potential at which 50\% of plant hydraulic conductivity is lost) (MPa)
\end{itemize}

%----------------------------------------------------------------------------------

\section{Child anthropometry}
\label{sec:snyderdata}

\textbf{File:} \file{anthropometry.csv}

Data were downloaded from \url{http://mreed.umtri.umich.edu/mreed/downloads.html}. Data include measurements of age, foot length, and height for 3898 children. These data are a small subset of many dozens of measurements on the same children, described in detail by Snyder (1977) (see above link for more information).

\textbf{Variables:}
\begin{itemize}
\item \code{age} - Age (years, converted from months in original dataset)
\item \code{gender} - Female or male
\item \code{foot\_length} - Total foot length (mm)
\item \code{height} - Total height (cm)
\end{itemize}



%----------------------------------------------------------------------------------

\section{Alphabet}
\label{sec:alphabetdata}

\textbf{File:} \file{alphabet.txt}

Lyrics for the song 'Alphabet Aerobics' by Blackalicious (3-2-1 Records, 1999). The rendition by Daniel Radcliffe is worth a watch (\url{https://www.youtube.com/watch?v=aKdV5FvXLuI}).



%----------------------------------------------------------------------------------
% TEMPLATE::
% \section{XXXX}
% \label{sec:XXXX}
% 
% \textbf{File:} \file{FILENAME}
%
% \textbf{Variables:}
% \begin{itemize}
% \item \code{} - first variable
% \item \code{} - second ...
% \item \code{}
% \item \code{}
% \item \code{}
% \item \code{}
% \end{itemize}



