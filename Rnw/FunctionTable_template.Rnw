\documentclass{article}

\begin{document}

% clearpage is preferred to newpage because it keeps floats from the 
% previous section from creeping on to the tables page.
\clearpage

%
% Table of functions -------------------------------------------------------------
\section{Functions used in this chapter}
\label{sec:funtab:PUT:NAME:NERE}

For functions not listed here, please refer to the index at the end of this book.

\keepXColumns
\begin{tabularx}{\linewidth}{SlX >{\ttfamily}Sl}
\label{tab:PUT:NAME:NERE}
\thead{Function} & \thead{What it does} & \thead{Example use} \\[-1ex] \toprule
\endhead
\midrule
\endfoot
\bottomrule
\endlastfoot
%
\fun{} & & \\
\fun{} & & \makecell{ \\ \hskip 2em \\ \hskip 4em }\\
%
\end{tabularx}


\end{document}