R Sessions : Generalized Linear Mixed Models
========================================================
*Kirk Barnett* - `r format(Sys.time(),"%Y-%m-%d")`


For this week I am looking at data I collected in my master's program on how tree species can affect the survival of different leaf-tying caterpillar species. These data are unbalance and not distributed normally. Luckily, this does matter as much when evaluating generalized linear mixed models! Plus we have a large number of observations; these conditions are perfect for a little glmm action. 

Let us read in the data:

```{r}
lfdata<-read.csv("lt2010.csv")
names(lfdata)
```

We will ignore year and subfamily and just focus on the other factors and their interactions.

Both caterpillar species are bivoltine so there are two generations per year. "tree" is the species of oak (*Quercus*) and there are four levels: Red, White, Black, and Post. "num" is the tree identity within "tree" species. "cat" is the species for caterpillar (not an actual cat). "treat" is the treatment applied to the branch of the tree; unbagged or bagged. "predpresent" is a binomial response of if there is a predator present in the leaf-tie or not when it was collected. "recover" is the binomial response of survived or not survived.

Now, originally I had "num" as a nonunique number nested within "tree" factor but that can create errors if you are unsure on how to nest so instead I just assinged each tree a unique number and made it a random factor in the model.

```{r}
## load the library

library(lme4)

## Comparing models with just AIC

lfdata$num<-as.factor(lfdata$num)
model0<-lmer(recover ~ generation + tree + cat + treat + predpresent + (1 | num) + generation:tree + generation:cat + generation:predpresent + tree:cat + cat:predpresent, data=lfdata, family=binomial)
model01<-lmer(recover ~ generation + tree + cat + treat + predpresent + (1 | num) + generation:tree + generation:cat + generation:predpresent + tree:cat, data=lfdata, family=binomial)
AIC(model0, model01)
```

First, there are several ways to compare GLMMs (forward, backwards, both ways) but here I have chosen to start with a completed model and then work my way backwards by eliminating factors that are not meaningful. The method I chose "*a priori*" was by comparing AIC values and picking the model with the lowest AIC. You also have the option to use the **anova(model0, model01)** and using the p value from the chi square. The p value in this case will have a different meaning depending on which direction you are going.

We have to load our libraries first and then turn the "num" variable into a factor because it is not continous.
Our response variable is binomial (yes or no) so we have **family=binomial** in the family slot. Note there are different choices for the link function which chooses which estimation method to use. The standard is laplace approximation, which is what I will be using; it is not very conservative though.

So we choose model01 because it has the lower AIC and drop the next term from the model.

```{r}
model02<-lmer(recover ~ generation + tree + cat + treat + predpresent + (1 | num) + generation:tree + generation:cat + generation:predpresent, data=lfdata, family=binomial)
AIC(model01, model02)
```

Again we keep the new model and then drop the next term.

```{r}
model03<-lmer(recover ~ generation + tree + cat + treat + predpresent + (1 | num) + generation:tree + generation:cat, data=lfdata, family=binomial)
AIC(model02, model03)
```

It is close here but for the sake of consistency I am going to keep it dropped, but note it can be argued to keep it in the model.... I think.

```{r}
model04<-lmer(recover ~ generation + tree + cat + treat + predpresent + (1 | num) + generation:tree, data=lfdata, family=binomial)
AIC(model03, model04)
```
Here we see that the AIC goes up when we drop  generation:cat from the model, so we keep it and eliminate the next term.

```{r}
model05<-lmer(recover ~ generation + tree + cat + treat + predpresent + (1 | num) + generation:cat, data=lfdata, family=binomial)
AIC(model03, model05)
```

Same thing when we drop **generation:tree**.

```{r}
model06<-lmer(recover ~ generation + tree + cat + treat + (1 | num) + generation:tree + generation:cat, data=lfdata, family=binomial)
AIC(model03, model06)
```

Model 6 is our new "best" model.

```{r}
model07<-lmer(recover ~ generation + tree + cat + (1 | num) + generation:tree + generation:cat, data=lfdata, family=binomial)
AIC(model06, model07)

model08<-lmer(recover ~ generation + tree + treat + (1 | num) + generation:tree + generation:cat, data=lfdata, family=binomial)
AIC(model06, model08)

model09<-lmer(recover ~ generation + cat + treat + (1 | num) + generation:tree + generation:cat, data=lfdata, family=binomial)
AIC(model06, model09)

model10<-lmer(recover ~ tree + cat + treat + (1 | num) + generation:tree + generation:cat, data=lfdata, family=binomial)
AIC(model06, model10)
```

So we come to this. Two models that have the same AIC value. What do we do? Well, in my opinion, although we could very well drop the term and not change the model, we have interactions that contain that term. So I would push to keep it in, even though you are penalized for every term you keep in the model. 

So model06 appears to be the best in this case, using this method and rational. (note the highly caveat-ed statement)

What do we see?

```{r}
summary(model06)
```

So we see we do have p values in this case, but you should be skeptical because they may not mean as much as you think. You will have to read deep into the psyche of Doug Bates by pouring over the r-sig-mixed-models@r-project.org mailing list but the wikidot on glmms says:

```By default, in keeping with the tradition in analysis of generalized linear models, lme4 and similar packages display the Wald Z-statistics for each parameter in the model summary. These have one big advantage: they're convenient to compute. However, they are asymptotic approximations, assuming both that (1) the sampling distributions of the parameters are multivariate normal (or equivalently that the log-likelihood surface is quadratic) and that (2) the sampling distribution of the log-likelihood is (proportional to) χ2. The second approximation is discussed further under "Degrees of freedom". The first assumption usually requires an even greater leap of faith, and is known to cause problems in some contexts (for binomial models failures of this assumption are called the Hauck-Donner effect), especially with extreme-valued parameters.```

So it is an approximation. Plus, we have one factor that has 4 levels and the intercept in this case only has the first level of "tree" in it; that being black. So we want something to simulate a posthoc test on a glmm and generate a number that can be interpreted as significance in relation to the other terms. 

At this point it should be noted that while glmms are flexible, they do suffer from overdispersion when the number of observations are low. Overdispersion is 
``` the case where the residual deviance is greater than the residual degrees of freedom. In otherwords, if φ (ratio of deviance and df) > 1 one speaks of overdispersion because the data have larger variance than expected under the assump tion of the chosen binomial distribution (Højsgaard and Halekoh). Crawley (2002) goes on to discuss overdispersion as the polite statistician’s version of Murphy’s Law. Overdispersion tends to arise because ‘you’ have not measured one or more of the factors that turned out to be important. It may also occur if you specify an incorrect error distribution. This means that the probability you are attempting to model is not constant within each cell, but behaves like a random variable. This in turn results in an inflated residual deviance. ``` 
Carruthers *et al.* 2008

So one way we could do this is by dividing the residual deviance by the residual degrees of freedom and if it is over 1.5 then it is considered overdispersed. This is kind of rough, however, so we want a better way of doing this. 

Introducing this cool function!

```{r}
disp_func<-function(modelfit){
  ## number of variance parameters in 
  ##   an n-by-n variance-covariance matrix
  vpars <- function(m) {
    nrow(m)*(nrow(m)+1)/2
  }
  model.df <- sum(sapply(VarCorr(modelfit),vpars))+length(fixef(modelfit))
  (rdf <- nrow(modelfit@frame)-model.df)
  rp <- residuals(modelfit)
  Pearson.chisq <- sum(rp^2)
  prat <- Pearson.chisq/rdf
  pval <- pchisq(Pearson.chisq, df=rdf, lower.tail=FALSE)
  c(chisq=Pearson.chisq,ratio=prat,rdf=rdf,p=pval)
}
```

The you call the function with the best model and you get:
```{r}
disp_func(model06)
```

So it is safe to say it is not overdispersed. But we wouldn't expect it to be anyway because of the large number of observations. **NOTE** that the p value here again is an **approximation**.

Back to that handy posthoc method. 

```{r}
library(LMERConvenienceFunctions)

model.posthoc06 <- mcposthoc.fnc(model06, var=list(ph1=c("tree")), mcmc=FALSE, nsim=10000, mc.cores=1)
```

Place your model in the first slot, the variable that has more than two levels must be in a list form even if it is single. nsim is the number of iterations, 10,000 is the minimum recommended and Windows is unable to use more than one core with R at the moment so you will have to leave it at that; linux can use multiple cores and I am unsure about Mac. If you have a rather complicated model with different terms with lots of levels then you will almost have to use multiple cores in order to use the function. Since the glmm uses an approximation, we get upper and lower p values which correspond to conservative and anti-conservative p values. Not exactly a solid number for significance but it is something... right?

```{r}
model.posthoc06
```

This prints the entire object, but if you were only curious about a few terms you can use:

```{r}
summary(model.posthoc06, term="generation")
```

You can also omit the `term="generation"` bit, and press 2 when a list of available terms is shown.

Nice! We see there IS an interaction of tree species and generation because some tree species show an effect on survival but others do not. 

