R Sessions : tips for quick plotting
========================================================
*Remko Duursma, John Drake and Chris Turbill* - `r format(Sys.time(),"%Y-%m-%d")`



Producing high quality figures in R is a lot of work. Sometimes, it is useful to be able to make a quick plot, particularly during the data exploration phase. There are many packages in R that contain some prefab plotting functions. In the HIE R manual, we introduced `bargraph.CI` in the `sciplot` package, for example, to quickly plot barplots of averages with standard errors.

This tutorial shows a few tips. This is just a small sample of the many available functions!

As a general rule, there is a trade-off between how easy it is to make complex graphs, and how easy it is to customize the look and feel of the graph. Using `base` graphics, it is not easy to make multi-panel plots with points, lines, legends, etc., but it is straightforward to customize all aspects. Using `qplot`, for example, it is stupendously easy to make complex graphs, but more work to customize everything. 

## Quick plots with `qplot` from the `ggplot2` package

The `ggplot2` package is very popular. If you ever read R blog entries, or R help posts, or similar, chances are you have seen examples using `ggplot2`. The attraction is that complex figures are easy to make. For full flexibility, take a look at the `ggplot` function. Here, we use the simple counterpart `qplot`. See here for a full reference:  http://ggplot2.org/

#### Coweeta data example

```{r qplot1}
# Some tree data
coweeta <- read.csv("coweeta.csv")

# ggplot2 package
library(ggplot2)

# A basic plot
qplot(DBH, height, data=coweeta)

# Colour by species - as we do with plot()
# Legend is added automatically.
qplot(DBH, height, data=coweeta, col=species)

# It is easy to add a smoother with a confidence region:
qplot(DBH, height, data=coweeta, geom=c("point","smooth"))

# And equally easy to split figure into panels by species.
# Here I add a linear model (regression) fit, with confidence region. 
# The syntax '.~species' means 'split columns by species'.
# You could try 'species~.', which will put each species in a row.
qplot(DBH, height, data=coweeta, geom=c("point","smooth"), method="lm", facets=.~species)
```

#### Dutch election data example

For this example, we will first show how to reshape data quickly, using the `reshape2` package. 

```{r qplot2}
# Data comes in 'wide' format.
# The column names are Dutch political parties, the numbers represent poll percentages.
elect <- read.csv("dutchelection.csv")
head(elect)

# Pretty much always, you want the data in 'long' format. The data will have three columns:
# Date, Party, Poll. Where Party is a factor variable.
# It's this easy:
library(reshape2)
elect2 <- melt(elect)
head(elect2)

# Now just rename the variables,
names(elect2)[2:3] <- c("Party","Poll")

# And finally make sure 'Date' is a Date variable,
elect2$Date <- as.Date(elect2$Date)
```

Now for some quick plots. 

```{r}
# If we were using the base package, we would do:
plot(Poll ~ Date, data=elect2, col=Party, pch=19)

# It would be visually useful to connect symbols with a line, for each party.
# Naively,
plot(Poll ~ Date, data=elect2, col=Party, pch=19, type='o')
# This has gone horribly wrong, base R simply keeps connecting the line, does not split it by Party!
# This is one of my main annoyances with base R.
# The solution, as I have shown before, is to write a loop that adds the lines.

# Or, use qplot
qplot(Date, Poll, data=elect2, geom=c("point","line"), col=Party)

# Boxplots are easy as well,
qplot(Party, Poll, data=elect2, geom="boxplot")

```

Finally, an example of multiple histograms. It is quite difficult to plot multiple histograms on one plot with the `base` graphics system. The package `epade` gives the very useful `histogram.ade`. But we can also use `qplot`:
```{r}
# Simulate some data:
norm1 <- rnorm(500, mean=50, sd=5)
norm2 <- rnorm(500, mean=70, sd=5)
df <- data.frame(g=rep(c("a","b"),each=500), Height=c(norm1,norm2))

# Histogram coloured by a factor:
qplot(Height, data=df, fill=g)
# Very nice. Too bad the default colours are atrocious, though.
```


## The `lattice` package

This package offers functionality similar to `ggplot2`, but may be easier in some respects. By default, it produces very ugly graphs, but does so quickly - which makes it useful when you are exploring your data and/or results from an analysis.


### Use `bwplot()` to look for interactions with categorical data 

This example shows how to make multiple boxplots, split by two factor variables. The one before the `|` will split the panel by that factor, the one after produces multiple panels.

```{r}
pupae <- read.csv("pupae.csv")
pupae$CO2_treatment <- as.factor(pupae$CO2_treatment)

library(lattice)
bwplot(PupalWeight~CO2_treatment|T_treatment,data=pupae)
```

### Use `xyplot` for continuous data

Here is an easy way to make scatter plots, split by a factor variable. 
We use the coweeta data from above.

```{r}
xyplot(height~DBH|species,data=coweeta)
```


## Climate maps with the `RNCEP` package

This example shows how to plot average climate variables across Australia. The script will download all data from an online repository. 

We then download species observations from `GBIF`, and add those to the plot.
```{r eval=TRUE,echo=FALSE,message=FALSE}
library(RNCEP)
library(dismo)
```
```{r, echo=TRUE, eval=FALSE}
# First download the data, and species occurrences.
#NCEP data description ftp://ftp.cpc.ncep.noaa.gov/wd51we/reanal/bams_paper.2001/reanl2.htm
library(RNCEP)

#download surface air temperature from NCEP from across Australia.
# variable air.sig995 is near surface T. Note that units are degK!
dat <- NCEP.gather(variable="air.sig995",level="surface",
                   months.minmax=c(1,12),years.minmax=c(2012,2012),
                   lat.southnorth=c(-39,-11),lon.westeast=c(113,154))


#aggregate all the data. Note that setting all to FALSE calculates the global mean
dat.avg <- NCEP.aggregate(dat,YEARS=FALSE,MONTHS=FALSE,DAYS=FALSE,HOURS=FALSE,fxn="mean")

#convert to degrees centigrade
dat.avg.C <- dat.avg-272.15

# Spatial data are free and easy to access using getData() within the raster package.
# species distributions are available from the global biodiversity inventory facility, gbif() within the dismo package
library(dismo)

#download observations of Eucalyptus grandis in the gbif.
grandis <- gbif("eucalyptus","grandis*")
```
```{r eval=FALSE, echo=FALSE}
save(dat.avg.C, grandis, file="cache.RData")
```

```{r echo=FALSE, eval=TRUE}
load("cache.RData")
library(RNCEP)
library(dismo)
```
```{r}
#plot the mean annual temperature across Australia in 2012
NCEP.vis.area(dat.avg.C,show.pts=FALSE,title.args=list(main="Mean Annual Temperature"))

#plot these observations on top of the map of mean annual temperature
points(grandis$lon,grandis$lat,pch=16,col="white",cex=0.6)
```

```{r eval=FALSE}
# Download elevation data for Australia
library(raster)
elev <- getData("alt",country="AUS")
```
```{r echo=FALSE, eval=FALSE}
save(elev, file="cache3.RData")
```
```{r echo=FALSE}
load("cache3.RData")
library(raster)
```
```{r rasterplot}
plot(elev)
```


## Visualizing linear model fits

Finally, three ways to quickly visualize a fitted linear model. All three methods work for `lm`, but only one seems to be tailored to `lme`. 


```{r echo=FALSE, message=FALSE}
library(nlme)
library(effects)
library(car)
library(visreg)
```
```{r eval=TRUE}
library(nlme)  # linear mixed effects models
library(effects)  # visualize effect sizes
library(car)   # companion to applied regression
library(visreg)  # visualize regression models 

data <- read.csv('hiblatsurv3.csv')
data$logmass <- log10(data$mass)
datarodents <- data[data$order =='Rodentia',]
databats <- data[data$order =='Chiroptera',]

# Visualize linear model fit
model1 <- lm(surv ~ tann + logmass, data = datarodents, na.action=na.exclude) # ignoring multiple data from each species
summary(model1)
crPlots(model1) 
visreg(model1, ask=FALSE) 
plot(allEffects(model1))

# Visualize linear mixed effects model fit?
rmodel1 <- lme(surv ~ tann + logmass, random = ~1|species, data = datarodents, na.action=na.exclude) 
#crPlots(rmodel1) # doesn't work with lme
#visreg(rmodel1, ask=FALSE) # error about levels of 'newdata'
plot(allEffects(rmodel1)) # works!
```


