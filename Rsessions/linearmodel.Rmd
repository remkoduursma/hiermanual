R Sessions : a linear model exercise
========================================================
*Remko Duursma* - `r format(Sys.time(),"%Y-%m-%d")`


This week we will look at fitting a linear model to some leaf gas exchange data from Teresa. Ignoring the science behind it, as we do, we will consider fitting a model of '
Photo' as a function of 'E', and deciding whether 'there are differences' with the CO2 treatment.

Let's prepare the data, and make a quick plot to inspect the data:
```{r}
# Raw data
teresa <- read.csv("teresa.spot.csv")

# Keep only three vars, rename the variables
teresa <- teresa[,c("CO2_Treat1","Photo","E")]
names(teresa)[1] <- "treatment"

# Make sure we know what order the levels of the treatment are in:
levels(teresa$treatment)

# So let's choose blue for ambient, red for elevated.
palette(c("blue","red"))

#
with(teresa, plot(E, Photo, pch=19, col=treatment))
```

It seems that there is 'certainly' a difference between the relationships for the two treatments. But do you expect this to show up in a difference in the slope between Photo and E? Or as an intercept?

Let's fit a linear model with `lm` to find out:
```{r}
# Linear model with E, treatment, and the interaction as independent variables
lmfit <- lm(Photo ~ E*treatment, data=teresa)

# Summary of the fit:
summary(lmfit)
```
Look at the 'coefficients' table in the summary statement. Four parameters are shown, they can be interpreted as:

1. The intercept for 'amb' 
2. The slope for 'amb'
3. The **difference** in the intercept for 'plus150', compared to 'amb'
4. The **difference** in the slope for 'plus150', compared to 'amb'.

It seems that neither 3) or 4) are significant, suggesting no treatment effects on either intercept or slope! 

It is often useful to refit the model where the coefficients are shown separate for each treatment, rather than relative to the first factor level. Here I also print the confidence intervals for the coefficients:
```{r}
# Linear model with E, treatment, and the interaction as independent variables
lmfit2 <- lm(Photo ~ treatment + treatment:E -1, data=teresa)

# Summary of the fit:
summary(lmfit2)

# Confidence intervals
confint(lmfit2)
```

This also helps our understanding : because the confidence intervals overlap for both the intercept (the first two coefficients), and the slopes (the next two), there is no treatment effect on either.

As often the case, an ANOVA of the linear model paints a slightly different picture. See section 6.4 in the HIE R manual for more background.

```{r}
# Suggests an intercept effect
anova(lmfit)
```

I always find it most instructive to plot the prediction of the linear model on top of the data, together with a confidence interval of the line. That way, we can directly visualize what the model coefficients are trying to tell us. In this case, we will find out something important about the intercept in the model.

```{r}
# Set up a regular sequence of numbers, for which 'Photo' is to be predicted from
xval <- seq(0, max(teresa$E), length=101)

# Two separate dataframes, one for each treatment/
amb_dfr <- data.frame(E=xval, treatment="amb")
ele_dfr <- data.frame(E=xval, treatment="plus150")

# Predictions of the model using 'predict.lm'
predamb <- as.data.frame(predict(lmfit, amb_dfr, interval="confidence"))
predele <- as.data.frame(predict(lmfit, ele_dfr, interval="confidence"))
  
# Plot. Set up the axis limits so that they start at 0, and go to the maximum.
windows(6,6)
with(teresa, plot(E, Photo, pch=19, col=treatment,
                  xlim=c(0, max(E)),
                  ylim=c(0, max(Photo))))
# Add the lines; the fit and lower and upper confidence intervals.
lines(xval, predamb$fit, col="blue", lwd=2)
lines(xval, predamb$lwr, col="blue", lwd=1, lty=3)
lines(xval, predamb$upr, col="blue", lwd=1, lty=3)

lines(xval, predele$fit, col="red", lwd=2)
lines(xval, predele$lwr, col="red", lwd=1, lty=3)
lines(xval, predele$upr, col="red", lwd=1, lty=3)
```  

Now, the intercept in a linear model is of course the value of the Y variable where X is zero. Note the very wide confidence intervals around the predictions for the intercept, **because the intercept is very far away from the data**. We know see why the intercept was not significant, but it says very little about the treatment difference **in the range of the data**. 

Recentering the data
-----------------------------

As a way out, let's instead fit the model where the intercept is right in the middle of our dataset, where we visually noticed a difference in the figure above.

Look at the following code, we use `scale` here to make new centered variables (which is exactly the same as `x - mean(x)`, by the way).
```{r}
# Rescaled E
teresa$Ecenter <- scale(teresa$E, center=TRUE, scale=FALSE)

# 
lmfit3 <- lm(Photo ~ Ecenter*treatment, data=teresa)

# Summary of the fit:
summary(lmfit3)
```

Now we have a significant intercept effect, of about 5 units (reasonable if you look at the above un-centered figure).


Excluding the intercept
-----------------------------------

Perhaps there is a good reason to exclude the intercept. It makes the test of a treatment effect easier, because now we only have to look at the slope.

Let's fit the model, inspect the summary and the plot with predictions. It seems that the model fits very poorly! Also remember to always make diagnostic plots with `plot(lmfit3)` in this case (not shown).
```{r}
# Fit a slope by treatment, but no intercept:
lmfit4 <- lm(Photo ~ treatment:E - 1, data=teresa)

# Summary and CI for the slopes
summary(lmfit4)
confint(lmfit4)

# Predict from model
predamb <- as.data.frame(predict(lmfit4, amb_dfr, interval="confidence"))
predele <- as.data.frame(predict(lmfit4, ele_dfr, interval="confidence"))

windows(6,6)
with(teresa, plot(E, Photo, pch=19, col=treatment,
                  xlim=c(0, max(E)),
                  ylim=c(0, max(Photo))))
lines(xval, predamb$fit, col="blue", lwd=2)
lines(xval, predamb$lwr, col="blue", lwd=1, lty=3)
lines(xval, predamb$upr, col="blue", lwd=1, lty=3)

lines(xval, predele$fit, col="red", lwd=2)
lines(xval, predele$lwr, col="red", lwd=1, lty=3)
lines(xval, predele$upr, col="red", lwd=1, lty=3)
```









