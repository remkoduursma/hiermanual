library(showtext)
library(sp)
library(scales)
library(png)

source("titlepage/titlepage_functions.R")

r <- require("fontr")
if(!r)devtools::install_github("yixuan/fontr", quick=TRUE)
library(fontr)

set.seed(103)
if(!exists("downloadedfonts")){
  download_googlefonts(200)
  downloadedfonts <- TRUE
}

# Main title page
make_titlepage(bigr="logo", bigrxwid=0.79, bigrywid=0.55, bigrcol="grey32", bigrcol2="grey52")
# 
# browseURL("titlepage/titlepage.pdf")


# Title page for solutions to exercises
# make_titlepage(bigrxwid=0.44, bigrywid=0.47, 
#                bigrfont="Noto Serif", bigrface="bold",
#                bigrtext="?",
#                bigrcol="grey32",
#                subtitle="Solutions to exercises",
#                authors="",
#                output="titlepage/titlepage_exercises.pdf")
# 
# browseURL("titlepage/titlepage_exercises.pdf")

# remove first page
# pdftk rnotes.pdf cat 2-end output rnotesnotitle.pdf
# collate
# pdftk titlepage/titlepage.pdf rnotesnotitle.pdf output rmanual.pdf
  

# pdftk 00_SolutionsExercises.pdf cat 2-end output tmp.pdf
# pdftk titlepage/titlepage_exercises.pdf tmp.pdf output rmanual_solutions_20160403.pdf


  

  
  
