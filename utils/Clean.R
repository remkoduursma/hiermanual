
del_ext <- c("tex","aux","idx","ilg","ind","out","toc","synctex.gz","log")
sapply(del_ext, function(x)unlink(Sys.glob(paste0("*.",x))))
