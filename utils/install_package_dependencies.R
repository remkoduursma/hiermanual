# You may have to run this 2-3 times for all packages to properly install

if(!"pacman" %in% .packages(TRUE)){
  install.packages("pacman")
}

pacman::p_load(Rcpp,caTools,gdata,gtools,gplots,stringr,stringi,
               Formula,splines,survival,multcomp,effects,
               mvtnorm,colorspace,RColorBrewer,lattice,permute,
               nnet,plotrix,moments,gplots,lubridate,
               sciplot,Hmisc,doBy,plyr,vegan,
               ade4,mvabund,foreign,car,epade,knitr,pander,
               broom,pixiedust,lubridate,
               lme4,scales,magicaxis,visreg,pbkrtest,
               lmerTest,LMERConvenienceFunctions,gridExtra,
               resample,git2r,nlstools,emmeans,pastecs)

